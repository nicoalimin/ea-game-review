package handler

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"
)

// User is the HTTP handler to perform handler-related logic on the users endpoint
type User struct {
	User core.CoreUser
}

// InsertUser marshals the body to the User struct and converts internal errors to a user-
// friendly one
func (u *User) InsertUser(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		return
	}

	var input core.User
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	err = u.User.InsertUser(&input)

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusCreated, input.ID)
	case errors.Is(err, core.ErrValidateUser):
		RespondWithError(w, http.StatusBadRequest, err.Error())
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

// LoadUserByID extracts a user by a single ID
func (u *User) LoadUserByID(w http.ResponseWriter, r *http.Request) {

	urlParams := mux.Vars(r)
	userID := urlParams["userID"]

	user, err := u.User.LoadUserByID(userID)

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusOK, (*user))
	case errors.Is(err, core.ErrUserNotFound):
		RespondWithError(w, http.StatusNotFound, "user does not exist")
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

// LoadUsers returns all users available in the system
func (u *User) LoadUsers(w http.ResponseWriter, r *http.Request) {

	users, err := u.User.LoadUsers()

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusOK, users)
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

// RemoveUser removes a user by a single ID
func (u *User) RemoveUser(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		return
	}

	urlParams := mux.Vars(r)
	userID := urlParams["userID"]

	err := u.User.RemoveUser(userID)

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusOK, userID)
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}
