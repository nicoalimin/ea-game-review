package handler

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"

	"github.com/golang/mock/gomock"
)

func Test_InsertReview(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"insertReviewSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusCreated,
		},
		"insertReviewInvalidError": {
			errorReturned:      core.ErrValidateReview,
			expectedStatusCode: http.StatusBadRequest,
		},
		"insertReviewUserNotExistError": {
			errorReturned:      core.ErrUserNotFound,
			expectedStatusCode: http.StatusBadRequest,
		},
		"insertReviewError": {
			errorReturned:      errors.New("This is an error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreReview := core.NewMockCoreReview(ctrl)

			handlerReview := Review{
				Review: mockCoreReview,
			}

			mockCoreReview.EXPECT().InsertReview(gomock.Any()).Times(1).Return(c.errorReturned)

			jsonInput := []byte(`{"content":"thanos' review","user_id":"user-id","title":"review-title"}`)
			r := httptest.NewRequest(http.MethodPost, "/reviews", bytes.NewBuffer(jsonInput))

			w := httptest.NewRecorder()

			handlerReview.InsertReview(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}

func Test_LoadReviewByID(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"loadReviewSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusOK,
		},
		"loadReviewNotFoundError": {
			errorReturned:      core.ErrReviewNotFound,
			expectedStatusCode: http.StatusNotFound,
		},
		"loadReviewGenericError": {
			errorReturned:      errors.New("unexpected error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreReview := core.NewMockCoreReview(ctrl)

			handlerReview := Review{
				Review: mockCoreReview,
			}

			if c.errorReturned == nil {
				mockCoreReview.EXPECT().LoadReviewByID(gomock.Any()).Times(1).Return(new(core.Review), c.errorReturned)
			} else {
				mockCoreReview.EXPECT().LoadReviewByID(gomock.Any()).Times(1).Return(nil, c.errorReturned)
			}

			r := httptest.NewRequest(http.MethodGet, "/reviews/review-id", nil)
			w := httptest.NewRecorder()

			handlerReview.LoadReviewByID(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}

func Test_LoadReviewByUserID(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"loadReviewSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusOK,
		},
		"loadReviewNotFoundError": {
			errorReturned:      core.ErrReviewNotFound,
			expectedStatusCode: http.StatusNotFound,
		},
		"loadReviewGenericError": {
			errorReturned:      errors.New("unexpected error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreReview := core.NewMockCoreReview(ctrl)

			handlerReview := Review{
				Review: mockCoreReview,
			}

			if c.errorReturned == nil {
				mockCoreReview.EXPECT().LoadReviewByUserID(gomock.Any()).Times(1).Return([]core.Review{core.Review{}}, c.errorReturned)
			} else {
				mockCoreReview.EXPECT().LoadReviewByUserID(gomock.Any()).Times(1).Return(nil, c.errorReturned)
			}

			r := httptest.NewRequest(http.MethodGet, "/users/user-id/reviews", nil)
			w := httptest.NewRecorder()

			handlerReview.LoadReviewByUserID(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}

func Test_LoadReviews(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"loadReviewSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusOK,
		},
		"loadReviewGenericError": {
			errorReturned:      errors.New("unexpected error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreReview := core.NewMockCoreReview(ctrl)

			handlerReview := Review{
				Review: mockCoreReview,
			}

			if c.errorReturned == nil {
				mockCoreReview.EXPECT().LoadReviews().Times(1).Return([]core.Review{}, c.errorReturned)
			} else {
				mockCoreReview.EXPECT().LoadReviews().Times(1).Return(nil, c.errorReturned)
			}

			r := httptest.NewRequest(http.MethodGet, "/reviews", nil)
			w := httptest.NewRecorder()

			handlerReview.LoadReviews(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}

func Test_RemoveReview(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"removeReviewSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusOK,
		},
		"removeReviewGenericError": {
			errorReturned:      errors.New("unexpected error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreReview := core.NewMockCoreReview(ctrl)

			handlerReview := Review{
				Review: mockCoreReview,
			}

			if c.errorReturned == nil {
				mockCoreReview.EXPECT().RemoveReview(gomock.Any()).Times(1).Return(c.errorReturned)
			} else {
				mockCoreReview.EXPECT().RemoveReview(gomock.Any()).Times(1).Return(c.errorReturned)
			}

			r := httptest.NewRequest(http.MethodDelete, "/reviews/review-id", nil)
			w := httptest.NewRecorder()

			handlerReview.RemoveReview(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}
