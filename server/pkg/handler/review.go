package handler

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"
)

// Review is the HTTP handler to perform handler-related logic on the reviews endpoint
type Review struct {
	Review core.CoreReview
}

// InsertReview marshals the body to the Review struct and converts internal errors to a review-
// friendly one
func (u *Review) InsertReview(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		return
	}

	var input core.Review
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	err = u.Review.InsertReview(&input)

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusCreated, input.ID)
	case errors.Is(err, core.ErrValidateReview):
		RespondWithError(w, http.StatusBadRequest, err.Error())
	case errors.Is(err, core.ErrUserNotFound):
		RespondWithError(w, http.StatusBadRequest, "User not found")
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

// LoadReviewByID extracts a review by a single ID
func (u *Review) LoadReviewByID(w http.ResponseWriter, r *http.Request) {

	urlParams := mux.Vars(r)
	reviewID := urlParams["reviewID"]

	review, err := u.Review.LoadReviewByID(reviewID)

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusOK, (*review))
	case errors.Is(err, core.ErrReviewNotFound):
		RespondWithError(w, http.StatusNotFound, "review does not exist")
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

// LoadReviewByUserID extracts a review by a single ID
func (u *Review) LoadReviewByUserID(w http.ResponseWriter, r *http.Request) {

	urlParams := mux.Vars(r)
	userID := urlParams["userID"]

	reviews, err := u.Review.LoadReviewByUserID(userID)

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusOK, reviews)
	case errors.Is(err, core.ErrReviewNotFound):
		RespondWithError(w, http.StatusNotFound, "review does not exist")
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

// LoadReviews returns all reviews available in the system
func (u *Review) LoadReviews(w http.ResponseWriter, r *http.Request) {

	reviews, err := u.Review.LoadReviews()

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusOK, reviews)
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

// RemoveReview removes a review by a single ID
func (u *Review) RemoveReview(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		return
	}

	urlParams := mux.Vars(r)
	reviewID := urlParams["reviewID"]

	err := u.Review.RemoveReview(reviewID)

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusOK, reviewID)
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}
