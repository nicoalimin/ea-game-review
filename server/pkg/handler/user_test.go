package handler

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"

	"github.com/golang/mock/gomock"
)

func Test_InsertUser(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"insertUserSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusCreated,
		},
		"insertUserError": {
			errorReturned:      errors.New("This is an error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreUser := core.NewMockCoreUser(ctrl)

			handlerUser := User{
				User: mockCoreUser,
			}

			mockCoreUser.EXPECT().InsertUser(gomock.Any()).Times(1).Return(c.errorReturned)

			jsonInput := []byte(`{"username":"thanos"}`)
			r := httptest.NewRequest(http.MethodPost, "/users", bytes.NewBuffer(jsonInput))

			w := httptest.NewRecorder()

			handlerUser.InsertUser(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}
func Test_LoadUserByID(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"loadUserSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusOK,
		},
		"loadUserNotFoundError": {
			errorReturned:      core.ErrUserNotFound,
			expectedStatusCode: http.StatusNotFound,
		},
		"loadUserGenericError": {
			errorReturned:      errors.New("unexpected error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreUser := core.NewMockCoreUser(ctrl)

			handlerUser := User{
				User: mockCoreUser,
			}

			if c.errorReturned == nil {
				mockCoreUser.EXPECT().LoadUserByID(gomock.Any()).Times(1).Return(new(core.User), c.errorReturned)
			} else {
				mockCoreUser.EXPECT().LoadUserByID(gomock.Any()).Times(1).Return(nil, c.errorReturned)
			}

			r := httptest.NewRequest(http.MethodGet, "/users/user-id", nil)
			w := httptest.NewRecorder()

			handlerUser.LoadUserByID(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}

func Test_LoadUsers(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"loadUserSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusOK,
		},
		"loadUserGenericError": {
			errorReturned:      errors.New("unexpected error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreUser := core.NewMockCoreUser(ctrl)

			handlerUser := User{
				User: mockCoreUser,
			}

			if c.errorReturned == nil {
				mockCoreUser.EXPECT().LoadUsers().Times(1).Return([]core.User{}, c.errorReturned)
			} else {
				mockCoreUser.EXPECT().LoadUsers().Times(1).Return(nil, c.errorReturned)
			}

			r := httptest.NewRequest(http.MethodGet, "/users", nil)
			w := httptest.NewRecorder()

			handlerUser.LoadUsers(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}

func Test_RemoveUser(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"removeUserSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusOK,
		},
		"removeUserGenericError": {
			errorReturned:      errors.New("unexpected error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreUser := core.NewMockCoreUser(ctrl)

			handlerUser := User{
				User: mockCoreUser,
			}

			if c.errorReturned == nil {
				mockCoreUser.EXPECT().RemoveUser(gomock.Any()).Times(1).Return(c.errorReturned)
			} else {
				mockCoreUser.EXPECT().RemoveUser(gomock.Any()).Times(1).Return(c.errorReturned)
			}

			r := httptest.NewRequest(http.MethodDelete, "/users/user-id", nil)
			w := httptest.NewRecorder()

			handlerUser.RemoveUser(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}
