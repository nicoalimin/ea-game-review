package handler

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"
)

// TODO: Add createdat timestamp

// Comment is the HTTP handler to perform handler-related logic on the comments endpoint
type Comment struct {
	Comment core.CoreComment
}

// InsertComment marshals the body to the Comment struct and converts internal errors to a
// comment-friendly one
func (u *Comment) InsertComment(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		return
	}

	var input core.Comment
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	err = u.Comment.InsertComment(&input)

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusCreated, input.ID)
	case errors.Is(err, core.ErrValidateComment):
		RespondWithError(w, http.StatusBadRequest, err.Error())
	case errors.Is(err, core.ErrUserNotFound):
		RespondWithError(w, http.StatusBadRequest, "User not found")
	case errors.Is(err, core.ErrReviewNotFound):
		RespondWithError(w, http.StatusBadRequest, "Review not found")
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

// LoadCommentByID extracts a comment by a single ID
func (u *Comment) LoadCommentByID(w http.ResponseWriter, r *http.Request) {

	urlParams := mux.Vars(r)
	commentID := urlParams["commentID"]

	comment, err := u.Comment.LoadCommentByID(commentID)

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusOK, (*comment))
	case errors.Is(err, core.ErrCommentNotFound):
		RespondWithError(w, http.StatusNotFound, "comment does not exist")
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

// LoadCommentByUserID extracts a comment by a single ID
func (u *Comment) LoadCommentByUserID(w http.ResponseWriter, r *http.Request) {

	urlParams := mux.Vars(r)
	userID := urlParams["userID"]

	comments, err := u.Comment.LoadCommentByUserID(userID)

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusOK, comments)
	case errors.Is(err, core.ErrCommentNotFound):
		RespondWithError(w, http.StatusNotFound, "comment does not exist")
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

// LoadCommentByReviewID extracts a comment by a single ID
func (u *Comment) LoadCommentByReviewID(w http.ResponseWriter, r *http.Request) {

	urlParams := mux.Vars(r)
	reviewID := urlParams["reviewID"]

	comments, err := u.Comment.LoadCommentByReviewID(reviewID)

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusOK, comments)
	case errors.Is(err, core.ErrCommentNotFound):
		RespondWithError(w, http.StatusNotFound, "comment does not exist")
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

// LoadComments returns all comments available in the system
func (u *Comment) LoadComments(w http.ResponseWriter, r *http.Request) {

	comments, err := u.Comment.LoadComments()

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusOK, comments)
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}

// RemoveComment removes a comment by a single ID
func (u *Comment) RemoveComment(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		return
	}

	urlParams := mux.Vars(r)
	commentID := urlParams["commentID"]

	err := u.Comment.RemoveComment(commentID)

	switch {
	case err == nil:
		RespondWithSuccess(w, http.StatusOK, commentID)
	default:
		RespondWithError(w, http.StatusInternalServerError, err.Error())
	}
}
