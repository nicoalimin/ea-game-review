package handler

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"

	"github.com/golang/mock/gomock"
)

func Test_InsertComment(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"insertCommentSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusCreated,
		},
		"insertCommentInvalidError": {
			errorReturned:      core.ErrValidateComment,
			expectedStatusCode: http.StatusBadRequest,
		},
		"insertCommentUserNotFoundError": {
			errorReturned:      core.ErrUserNotFound,
			expectedStatusCode: http.StatusBadRequest,
		},
		"insertCommentReviewNotFoundError": {
			errorReturned:      core.ErrReviewNotFound,
			expectedStatusCode: http.StatusBadRequest,
		},
		"insertCommentError": {
			errorReturned:      errors.New("This is an error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreComment := core.NewMockCoreComment(ctrl)

			handlerComment := Comment{
				Comment: mockCoreComment,
			}

			mockCoreComment.EXPECT().InsertComment(gomock.Any()).Times(1).Return(c.errorReturned)

			jsonInput := []byte(`{"content":"thanos' comment","user_id":"user-id","review_id":"review-id"}`)
			r := httptest.NewRequest(http.MethodPost, "/comments", bytes.NewBuffer(jsonInput))

			w := httptest.NewRecorder()

			handlerComment.InsertComment(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}

func Test_LoadCommentByID(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"loadCommentSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusOK,
		},
		"loadCommentNotFoundError": {
			errorReturned:      core.ErrCommentNotFound,
			expectedStatusCode: http.StatusNotFound,
		},
		"loadCommentGenericError": {
			errorReturned:      errors.New("unexpected error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreComment := core.NewMockCoreComment(ctrl)

			handlerComment := Comment{
				Comment: mockCoreComment,
			}

			if c.errorReturned == nil {
				mockCoreComment.EXPECT().LoadCommentByID(gomock.Any()).Times(1).Return(new(core.Comment), c.errorReturned)
			} else {
				mockCoreComment.EXPECT().LoadCommentByID(gomock.Any()).Times(1).Return(nil, c.errorReturned)
			}

			r := httptest.NewRequest(http.MethodGet, "/comments/comment-id", nil)
			w := httptest.NewRecorder()

			handlerComment.LoadCommentByID(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}

func Test_LoadCommentByUserID(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"loadCommentSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusOK,
		},
		"loadCommentNotFoundError": {
			errorReturned:      core.ErrCommentNotFound,
			expectedStatusCode: http.StatusNotFound,
		},
		"loadCommentGenericError": {
			errorReturned:      errors.New("unexpected error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreComment := core.NewMockCoreComment(ctrl)

			handlerComment := Comment{
				Comment: mockCoreComment,
			}

			if c.errorReturned == nil {
				mockCoreComment.EXPECT().LoadCommentByUserID(gomock.Any()).Times(1).Return([]core.Comment{core.Comment{}}, c.errorReturned)
			} else {
				mockCoreComment.EXPECT().LoadCommentByUserID(gomock.Any()).Times(1).Return(nil, c.errorReturned)
			}

			r := httptest.NewRequest(http.MethodGet, "/users/user-id/comments", nil)
			w := httptest.NewRecorder()

			handlerComment.LoadCommentByUserID(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}

func Test_LoadCommentByReviewID(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"loadCommentSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusOK,
		},
		"loadCommentNotFoundError": {
			errorReturned:      core.ErrCommentNotFound,
			expectedStatusCode: http.StatusNotFound,
		},
		"loadCommentGenericError": {
			errorReturned:      errors.New("unexpected error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreComment := core.NewMockCoreComment(ctrl)

			handlerComment := Comment{
				Comment: mockCoreComment,
			}

			if c.errorReturned == nil {
				mockCoreComment.EXPECT().LoadCommentByReviewID(gomock.Any()).Times(1).Return([]core.Comment{core.Comment{}}, c.errorReturned)
			} else {
				mockCoreComment.EXPECT().LoadCommentByReviewID(gomock.Any()).Times(1).Return(nil, c.errorReturned)
			}

			r := httptest.NewRequest(http.MethodGet, "/users/user-id/comments", nil)
			w := httptest.NewRecorder()

			handlerComment.LoadCommentByReviewID(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}

func Test_LoadComments(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"loadCommentSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusOK,
		},
		"loadCommentGenericError": {
			errorReturned:      errors.New("unexpected error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreComment := core.NewMockCoreComment(ctrl)

			handlerComment := Comment{
				Comment: mockCoreComment,
			}

			if c.errorReturned == nil {
				mockCoreComment.EXPECT().LoadComments().Times(1).Return([]core.Comment{}, c.errorReturned)
			} else {
				mockCoreComment.EXPECT().LoadComments().Times(1).Return(nil, c.errorReturned)
			}

			r := httptest.NewRequest(http.MethodGet, "/comments", nil)
			w := httptest.NewRecorder()

			handlerComment.LoadComments(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}

func Test_RemoveComment(t *testing.T) {
	cases := map[string]struct {
		errorReturned      error
		expectedStatusCode int
	}{
		"removeCommentSuccess": {
			errorReturned:      nil,
			expectedStatusCode: http.StatusOK,
		},
		"removeCommentGenericError": {
			errorReturned:      errors.New("unexpected error"),
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockCoreComment := core.NewMockCoreComment(ctrl)

			handlerComment := Comment{
				Comment: mockCoreComment,
			}

			if c.errorReturned == nil {
				mockCoreComment.EXPECT().RemoveComment(gomock.Any()).Times(1).Return(c.errorReturned)
			} else {
				mockCoreComment.EXPECT().RemoveComment(gomock.Any()).Times(1).Return(c.errorReturned)
			}

			r := httptest.NewRequest(http.MethodDelete, "/comments/comment-id", nil)
			w := httptest.NewRecorder()

			handlerComment.RemoveComment(w, r)

			if w.Code != c.expectedStatusCode {
				t.Errorf("expected status code doesn't match actual. \nGot: %d \nWant: %d", w.Code, c.expectedStatusCode)
			}
		})
	}
}
