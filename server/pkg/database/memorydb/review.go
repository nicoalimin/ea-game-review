package memorydb

import (
	"strconv"
	"sync"
	"time"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"
)

// compile-time check for concrete implementation of interface
var _ core.DatabaseReview = (*DatabaseReview)(nil)

// DatabaseReview is an in-memory, thread-safe implementation of the DatabaseReview
// interface
type DatabaseReview struct {
	reviewByID map[string]core.Review
	mutex      sync.RWMutex
}

// NewDatabaseReview returns an in-memory implementation of the Review Database with applied options
func NewDatabaseReview(options ...DatabaseReviewOption) *DatabaseReview {
	dr := &DatabaseReview{
		reviewByID: make(map[string]core.Review),
	}

	for _, applyOpt := range options {
		applyOpt(dr)
	}

	return dr
}

// DatabaseReviewOption returns a method that is used to apply optional parameters to the constructor
type DatabaseReviewOption func(*DatabaseReview)

// OptInitialReviews initializes the Database Review struct with an initial value. This
// could be useful if we want to seed in data to the in-memory implementation because it
// will get lost when the backend restarts
func OptInitialReviews(reviews map[string]core.Review) DatabaseReviewOption {
	return func(dr *DatabaseReview) {
		if reviews == nil {
			reviews = make(map[string]core.Review)
		}
		dr.reviewByID = reviews
	}
}

// InsertReview stores the given review in memory. If a review with the same ID already exists,
// it will be replaced
func (dr *DatabaseReview) InsertReview(review *core.Review) error {
	dr.mutex.Lock()
	defer dr.mutex.Unlock()

	numID := len(dr.reviewByID)
	review.ID = strconv.Itoa(numID + 1)
	review.CreatedAt = time.Now().UTC().Format("2006-01-02T15:04:05-0700")

	dr.reviewByID[review.ID] = (*review)
	return nil
}

// LoadReview returns the Review entity given the ID. If no Reviews are found with the
// given ID, it returns nil
func (dr *DatabaseReview) LoadReview(reviewFilter core.ReviewFilter) ([]core.Review, error) {
	dr.mutex.Lock()
	defer dr.mutex.Unlock()

	// Gather all reviews
	reviews := []core.Review{}
	for _, v := range dr.reviewByID {
		reviews = append(reviews, v)
	}

	// Filter by ID if present
	if reviewFilter.ID != "" {
		filteredReviews := []core.Review{}
		for _, v := range reviews {
			if v.ID != reviewFilter.ID {
				continue
			}
			filteredReviews = append(filteredReviews, v)
		}
		reviews = filteredReviews
	}

	// Filter by User ID if present
	if reviewFilter.UserID != "" {
		filteredReviews := []core.Review{}
		for _, v := range reviews {
			if v.UserID != reviewFilter.UserID {
				continue
			}
			filteredReviews = append(filteredReviews, v)
		}
		reviews = filteredReviews
	}

	if len(reviews) == 0 {
		return nil, nil
	}

	return reviews, nil
}

// RemoveReview deletes the Review in the in-memory implementation by the ID. If there is
// no review with the desired ID, it returns nil
func (dr *DatabaseReview) RemoveReview(reviewID string) error {
	dr.mutex.Lock()
	defer dr.mutex.Unlock()

	delete(dr.reviewByID, reviewID)
	return nil
}
