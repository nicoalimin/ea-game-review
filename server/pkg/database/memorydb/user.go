package memorydb

import (
	"strconv"
	"sync"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"
)

// compile-time check for concrete implementation of interface
var _ core.DatabaseUser = (*DatabaseUser)(nil)

// DatabaseUser is an in-memory, thread-safe implementation of the DatabaseUser interface
type DatabaseUser struct {
	userByID map[string]core.User
	mutex    sync.RWMutex
}

// NewDatabaseUser returns an in-memory implementation of the User Database.
func NewDatabaseUser(options ...DatabaseUserOption) *DatabaseUser {
	du := &DatabaseUser{
		userByID: make(map[string]core.User),
	}

	for _, applyOpt := range options {
		applyOpt(du)
	}

	return du
}

// DatabaseUserOption returns a method that is used to apply optional parameters to the constructor
type DatabaseUserOption func(*DatabaseUser)

// OptInitialUsers initializes the Database User struct with an initial value. This could
// be useful if we want to seed in data to the in-memory implementation because it will
// get lost when the backend restarts
func OptInitialUsers(users map[string]core.User) DatabaseUserOption {
	return func(du *DatabaseUser) {
		if users == nil {
			users = make(map[string]core.User)
		}
		du.userByID = users
	}
}

// InsertUser stores the given user in memory. If a user with the same ID already exists,
// it will be replaced
func (du *DatabaseUser) InsertUser(user *core.User) error {
	du.mutex.Lock()
	defer du.mutex.Unlock()

	numID := len(du.userByID)
	user.ID = strconv.Itoa(numID + 1)

	du.userByID[user.ID] = (*user)
	return nil
}

// LoadUser returns the User entity given the ID. If no Users are found with the given ID,
// it returns nil
func (du *DatabaseUser) LoadUser(userFilter core.UserFilter) ([]core.User, error) {
	du.mutex.Lock()
	defer du.mutex.Unlock()

	if userFilter.ID != "" {
		user, ok := du.userByID[userFilter.ID]
		if !ok {
			return nil, nil
		}
		return []core.User{user}, nil
	}

	users := []core.User{}
	for _, v := range du.userByID {
		users = append(users, v)
	}

	if len(users) == 0 {
		return nil, nil
	}

	return users, nil
}

// RemoveUser deletes the User in the in-memory implementation by the ID. If there is no
// user with the desired ID, it returns nil
func (du *DatabaseUser) RemoveUser(userID string) error {
	du.mutex.Lock()
	defer du.mutex.Unlock()

	delete(du.userByID, userID)
	return nil
}
