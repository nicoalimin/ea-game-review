package memorydb

import (
	"testing"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"
	"gitlab.com/nicoalimin/ea-game-review/server/pkg/utils"
)

// castCommentToInterfaceHelper is a helper function to case a concrete type Comment to an
// anonymous struct for easy comparison
func castCommentToInterfaceHelper(comments []core.Comment) []interface{} {
	interfaces := []interface{}{}
	for _, v := range comments {
		interfaces = append(interfaces, v)
	}
	return interfaces
}

func Test_InsertComment_Success(t *testing.T) {
	databaseComment := NewDatabaseComment()

	desiredComment := core.Comment{}
	err := databaseComment.InsertComment(&desiredComment)
	if err != nil {
		t.Errorf("Expected no errors when Inserting a comment. \nGot: %v", err)
	}
}

func Test_LoadComment_NonExistent_Success(t *testing.T) {
	databaseComment := NewDatabaseComment()

	comment, err := databaseComment.LoadComment(core.CommentFilter{
		ID: "comment-id",
	})
	if err != nil {
		t.Errorf("Expected no errors when loading a non-existent comment. \nGot: %v", err)
	}
	if comment != nil {
		t.Errorf("Expected comment returned to be nil when looking a non-existent comment. \nGot: %v", comment)
	}
}

func commentFactory(ID, UserID, ReviewID string) core.Comment {
	return core.Comment{
		ID:        ID,
		UserID:    UserID,
		ReviewID:  ReviewID,
		Content:   "comment-content",
		CreatedAt: "",
	}
}

func Test_LoadComments(t *testing.T) {
	commentID1 := "comment-id-1"
	commentUserID1 := "user-id-1"
	reviewID1 := "review-id-1"
	comment1 := commentFactory(commentID1, commentUserID1, reviewID1)

	commentID2 := "comment-id-2"
	reviewID2 := "review-id-2"
	comment2 := commentFactory(commentID2, commentUserID1, reviewID2)

	commentID3 := "comment-id-3"
	commentUserID3 := "user-id-3"
	reviewID3 := "review-id-3"
	comment3 := commentFactory(commentID3, commentUserID3, reviewID3)

	commentsMap := map[string]core.Comment{
		comment1.ID: comment1,
		comment2.ID: comment2,
		comment3.ID: comment3,
	}

	cases := map[string]struct {
		commentFilter    core.CommentFilter
		expectedComments []core.Comment
	}{
		"returnAllCommentsSuccess": {
			commentFilter:    core.CommentFilter{},
			expectedComments: []core.Comment{comment1, comment2, comment3},
		},
		"returnCommentsByIDSuccess": {
			commentFilter:    core.CommentFilter{ID: commentID1},
			expectedComments: []core.Comment{comment1},
		},
		"returnCommentsByUserIDSuccess": {
			commentFilter:    core.CommentFilter{UserID: commentUserID1},
			expectedComments: []core.Comment{comment1, comment2},
		},
		"returnCommentsByIDAndUserIDSuccess": {
			commentFilter:    core.CommentFilter{ID: commentID1, UserID: commentUserID1},
			expectedComments: []core.Comment{comment1},
		},
		"returnCommentsByReviewIDSuccess": {
			commentFilter:    core.CommentFilter{ReviewID: reviewID1},
			expectedComments: []core.Comment{comment1},
		},
		"returnCommentsNotFound": {
			commentFilter:    core.CommentFilter{ID: "non-existent-ID"},
			expectedComments: nil,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			databaseComment := NewDatabaseComment(OptInitialComments(commentsMap))

			actualComments, err := databaseComment.LoadComment(c.commentFilter)
			if err != nil {
				t.Errorf("Expected no errors when loading comments. \nGot: %v", err)
			}
			if !utils.Equal(castCommentToInterfaceHelper(c.expectedComments), castCommentToInterfaceHelper(actualComments)) {
				t.Errorf("Expected comment doesn't match actual. \nGot: %v \nWant: %v", actualComments, c.expectedComments)
			}
		})
	}
}

func Test_RemoveComment_Success(t *testing.T) {
	initialCommentID := "initial-id"
	initialComment := core.Comment{
		ID:      initialCommentID,
		Content: "initial-Content",
	}
	initialComments := map[string]core.Comment{initialCommentID: initialComment}
	databaseComment := NewDatabaseComment(OptInitialComments(initialComments))

	err := databaseComment.RemoveComment(initialCommentID)

	if err != nil {
		t.Errorf("Expected no errors removing an existing comment. \nGot: %v", err)
	}

}
