package memorydb

import (
	"testing"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"
	"gitlab.com/nicoalimin/ea-game-review/server/pkg/utils"
)

// castReviewToInterfaceHelper is a helper function to case a concrete type Review to an
// anonymous struct for easy comparison
func castReviewToInterfaceHelper(reviews []core.Review) []interface{} {
	interfaces := []interface{}{}
	for _, v := range reviews {
		interfaces = append(interfaces, v)
	}
	return interfaces
}

func Test_InsertReview_Success(t *testing.T) {
	databaseReview := NewDatabaseReview()

	desiredReview := core.Review{}
	err := databaseReview.InsertReview(&desiredReview)
	if err != nil {
		t.Errorf("Expected no errors when Inserting a review. \nGot: %v", err)
	}
}

func Test_LoadReview_NonExistent_Success(t *testing.T) {
	databaseReview := NewDatabaseReview()

	review, err := databaseReview.LoadReview(core.ReviewFilter{
		ID: "review-id",
	})
	if err != nil {
		t.Errorf("Expected no errors when loading a non-existent review. \nGot: %v", err)
	}
	if review != nil {
		t.Errorf("Expected review returned to be nil when looking a non-existent review. \nGot: %v", review)
	}
}

func reviewFactory(ID, UserID string) core.Review {
	return core.Review{
		ID:        ID,
		UserID:    UserID,
		Title:     "review-title",
		Content:   "review-content",
		CreatedAt: "",
	}
}

func Test_LoadReviews(t *testing.T) {
	reviewID1 := "review-id-1"
	reviewUserID1 := "user-id-1"
	review1 := reviewFactory(reviewID1, reviewUserID1)

	reviewID2 := "review-id-2"
	review2 := reviewFactory(reviewID2, reviewUserID1)

	reviewID3 := "review-id-3"
	reviewUserID3 := "user-id-3"
	review3 := reviewFactory(reviewID3, reviewUserID3)

	reviewsMap := map[string]core.Review{
		review1.ID: review1,
		review2.ID: review2,
		review3.ID: review3,
	}

	cases := map[string]struct {
		reviewFilter    core.ReviewFilter
		expectedReviews []core.Review
	}{
		"returnAllReviewsSuccess": {
			reviewFilter:    core.ReviewFilter{},
			expectedReviews: []core.Review{review1, review2, review3},
		},
		"returnReviewsByIDSuccess": {
			reviewFilter:    core.ReviewFilter{ID: reviewID1},
			expectedReviews: []core.Review{review1},
		},
		"returnReviewsByUserIDSuccess": {
			reviewFilter:    core.ReviewFilter{UserID: reviewUserID1},
			expectedReviews: []core.Review{review1, review2},
		},
		"returnReviewsByIDAndUserIDSuccess": {
			reviewFilter:    core.ReviewFilter{ID: reviewID1, UserID: reviewUserID1},
			expectedReviews: []core.Review{review1},
		},
		"returnReviewsNotFound": {
			reviewFilter:    core.ReviewFilter{ID: "non-existent-ID"},
			expectedReviews: nil,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			databaseReview := NewDatabaseReview(OptInitialReviews(reviewsMap))

			actualReviews, err := databaseReview.LoadReview(c.reviewFilter)
			if err != nil {
				t.Errorf("Expected no errors when loading reviews. \nGot: %v", err)
			}
			if !utils.Equal(castReviewToInterfaceHelper(c.expectedReviews), castReviewToInterfaceHelper(actualReviews)) {
				t.Errorf("Expected review doesn't match actual. \nGot: %v \nWant: %v", actualReviews, c.expectedReviews)
			}
		})
	}
}

func Test_RemoveReview_Success(t *testing.T) {
	initialReviewID := "initial-id"
	initialReview := core.Review{
		ID:      initialReviewID,
		Content: "initial-Content",
	}
	initialReviews := map[string]core.Review{initialReviewID: initialReview}
	databaseReview := NewDatabaseReview(OptInitialReviews(initialReviews))

	err := databaseReview.RemoveReview(initialReviewID)

	if err != nil {
		t.Errorf("Expected no errors removing an existing review. \nGot: %v", err)
	}

}
