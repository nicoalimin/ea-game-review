package memorydb

import (
	"strconv"
	"sync"
	"time"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"
)

// compile-time check for concrete implementation of interface
var _ core.DatabaseComment = (*DatabaseComment)(nil)

// DatabaseComment is an in-memory, thread-safe implementation of the DatabaseComment
// interface
type DatabaseComment struct {
	commentByID map[string]core.Comment
	mutex       sync.RWMutex
}

// NewDatabaseComment returns an in-memory implementation of the Comment Database with
// applied options
func NewDatabaseComment(options ...DatabaseCommentOption) *DatabaseComment {
	dr := &DatabaseComment{
		commentByID: make(map[string]core.Comment),
	}

	for _, applyOpt := range options {
		applyOpt(dr)
	}

	return dr
}

// DatabaseCommentOption returns a method that is used to apply optional parameters to the constructor
type DatabaseCommentOption func(*DatabaseComment)

// OptInitialComments initializes the Database Comment struct with an initial value. This
// could be useful if we want to seed in data to the in-memory implementation because it
// will get lost when the backend restarts
func OptInitialComments(comments map[string]core.Comment) DatabaseCommentOption {
	return func(dr *DatabaseComment) {
		if comments == nil {
			comments = make(map[string]core.Comment)
		}
		dr.commentByID = comments
	}
}

// InsertComment stores the given comment in memory. If a comment with the same ID already
// exists, it will be replaced
func (dc *DatabaseComment) InsertComment(comment *core.Comment) error {
	dc.mutex.Lock()
	defer dc.mutex.Unlock()

	numID := len(dc.commentByID)
	comment.ID = strconv.Itoa(numID + 1)
	comment.CreatedAt = time.Now().UTC().Format("2006-01-02T15:04:05-0700")

	dc.commentByID[comment.ID] = (*comment)
	return nil
}

// LoadComment returns the Comment entity given the ID. If no Comments are found with the
// given ID, it returns nil
func (dc *DatabaseComment) LoadComment(commentFilter core.CommentFilter) ([]core.Comment, error) {
	dc.mutex.Lock()
	defer dc.mutex.Unlock()

	// Gather all comments
	comments := []core.Comment{}
	for _, v := range dc.commentByID {
		comments = append(comments, v)
	}

	// Filter by ID if present
	if commentFilter.ID != "" {
		filteredComments := []core.Comment{}
		for _, v := range comments {
			if v.ID != commentFilter.ID {
				continue
			}
			filteredComments = append(filteredComments, v)
		}
		comments = filteredComments
	}

	// Filter by User ID if present
	if commentFilter.UserID != "" {
		filteredComments := []core.Comment{}
		for _, v := range comments {
			if v.UserID != commentFilter.UserID {
				continue
			}
			filteredComments = append(filteredComments, v)
		}
		comments = filteredComments
	}

	// Filter by Review ID if present
	if commentFilter.ReviewID != "" {
		filteredComments := []core.Comment{}
		for _, v := range comments {
			if v.ReviewID != commentFilter.ReviewID {
				continue
			}
			filteredComments = append(filteredComments, v)
		}
		comments = filteredComments
	}

	if len(comments) == 0 {
		return nil, nil
	}

	return comments, nil
}

// RemoveComment deletes the Comment in the in-memory implementation by the ID. If there is
// no comment with the desired ID, it returns nil
func (dc *DatabaseComment) RemoveComment(commentID string) error {
	dc.mutex.Lock()
	defer dc.mutex.Unlock()

	delete(dc.commentByID, commentID)
	return nil
}
