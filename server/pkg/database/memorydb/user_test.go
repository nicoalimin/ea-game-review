package memorydb

import (
	"testing"

	"github.com/go-test/deep"
	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"
)

func Test_InsertUser_Success(t *testing.T) {
	databaseUser := NewDatabaseUser()

	desiredUser := core.User{}
	err := databaseUser.InsertUser(&desiredUser)
	if err != nil {
		t.Errorf("Expected no errors when Inserting a user. \nGot: %v", err)
	}
}

func Test_LoadUser_NonExistent_Success(t *testing.T) {
	databaseUser := NewDatabaseUser()

	user, err := databaseUser.LoadUser(core.UserFilter{
		ID: "user-id",
	})
	if err != nil {
		t.Errorf("Expected no errors when loading a non-existent user. \nGot: %v", err)
	}
	if user != nil {
		t.Errorf("Expected user returned to be nil when looking a non-existent user. \nGot: %v", user)
	}
}

func Test_LoadUser_Exist_Success(t *testing.T) {
	initialUserID := "initial-id"
	initialUser := core.User{
		ID:       initialUserID,
		Username: "initial-username",
	}
	initialUsers := map[string]core.User{initialUserID: initialUser}
	databaseUser := NewDatabaseUser(OptInitialUsers(initialUsers))

	actualUsers, err := databaseUser.LoadUser(core.UserFilter{
		ID: initialUserID,
	})
	if err != nil {
		t.Errorf("Expected no errors when loading an existent user. \nGot: %v", err)
	}
	if diff := deep.Equal([]core.User{initialUser}, actualUsers); len(diff) != 0 {
		t.Errorf("Expected user doesn't match actual. \nGot: %v \nWant: %v", actualUsers, []core.User{initialUser})
	}
}

func Test_LoadUser_Success(t *testing.T) {
	initialUserID := "initial-id"
	initialUser := core.User{
		ID:       initialUserID,
		Username: "initial-username",
	}
	initialUsers := map[string]core.User{initialUserID: initialUser}
	databaseUser := NewDatabaseUser(OptInitialUsers(initialUsers))

	actualUsers, err := databaseUser.LoadUser(core.UserFilter{})
	if err != nil {
		t.Errorf("Expected no errors when loading all users. \nGot: %v", err)
	}

	if diff := deep.Equal([]core.User{initialUser}, actualUsers); len(diff) != 0 {
		t.Errorf("Expected user doesn't match actual. \nGot: %v \nWant: %v", actualUsers, []core.User{initialUser})
	}
}

func Test_RemoveUser_Success(t *testing.T) {
	initialUserID := "initial-id"
	initialUser := core.User{
		ID:       initialUserID,
		Username: "initial-username",
	}
	initialUsers := map[string]core.User{initialUserID: initialUser}
	databaseUser := NewDatabaseUser(OptInitialUsers(initialUsers))

	err := databaseUser.RemoveUser(initialUserID)

	if err != nil {
		t.Errorf("Expected no errors removing an existing user. \nGot: %v", err)
	}

}
