package utils

import (
	"testing"
)

func Test_Equal(t *testing.T) {
	type testStruct struct {
		data string
	}

	cases := map[string]struct {
		caseA          []interface{}
		caseB          []interface{}
		expectedResult bool
	}{
		"compareEqualStringsSuccess": {
			caseA:          []interface{}{"blah"},
			caseB:          []interface{}{"blah"},
			expectedResult: true,
		},
		"compareEqualIntsSuccess": {
			caseA:          []interface{}{1},
			caseB:          []interface{}{1},
			expectedResult: true,
		},
		"compareNonEqualStringsFailure": {
			caseA:          []interface{}{"blah"},
			caseB:          []interface{}{"avengers"},
			expectedResult: false,
		},
		"compareNonEqualIntsFailure": {
			caseA:          []interface{}{1},
			caseB:          []interface{}{1000000000},
			expectedResult: false,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			interfacesA := make([]interface{}, len(c.caseA))
			for i, v := range c.caseA {
				interfacesA[i] = v
			}

			interfacesB := make([]interface{}, len(c.caseB))
			for i, v := range c.caseB {
				interfacesB[i] = v
			}

			actualResult := Equal(interfacesA, interfacesB)
			if actualResult != c.expectedResult {
				t.Errorf("actual result does not match expected. \nGot: %t, \nWant: %t", actualResult, c.expectedResult)
			}
		})
	}
}
