package utils

import (
	"github.com/go-test/deep"
)

// Equal tells whether a and b contain the same elements.
// A nil argument is equivalent to an empty slice.
func Equal(a, b []interface{}) bool {
	if len(a) != len(b) {
		return false
	}
	for _, v := range a {
		found := false
		for _, w := range b {
			if diff := deep.Equal(v, w); len(diff) == 0 {
				found = true
			}
		}
		if !found {
			return false
		}
	}
	return true
}
