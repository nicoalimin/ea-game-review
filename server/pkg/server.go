package server

import (
	"fmt"
	"net/http"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/seed"

	"gitlab.com/nicoalimin/ea-game-review/server/conf"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/database/memorydb"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/handler"

	"github.com/gorilla/mux"
)

var (
	config conf.Conf
)

func init() {
	config = conf.NewConf()
}

func loadRoutes(r *mux.Router) {

	// Seed the data
	users, reviews, comments := seed.GenerateSeed()

	// Initialize Database Layer
	userDatabaseMemory := memorydb.NewDatabaseUser(memorydb.OptInitialUsers(users))
	reviewDatabaseMemory := memorydb.NewDatabaseReview(memorydb.OptInitialReviews(reviews))
	commentDatabaseMemory := memorydb.NewDatabaseComment(memorydb.OptInitialComments(comments))

	// Initialize Core Business Logic Layer
	userCore := core.ServiceUser{
		DatabaseUser: userDatabaseMemory,
	}
	reviewCore := core.ServiceReview{
		DatabaseReview: reviewDatabaseMemory,
		DatabaseUser:   userDatabaseMemory,
	}
	commentCore := core.ServiceComment{
		DatabaseComment: commentDatabaseMemory,
		DatabaseUser:    userDatabaseMemory,
		DatabaseReview:  reviewDatabaseMemory,
	}

	// Initialize Handlers Layer
	handlerHealth := handler.Health{}
	docsHandler := handler.Docs{
		DocsPathIndex: "./docs/index.html",
		DocsPathYaml:  "./docs/swagger.yaml",
	}
	userHandler := handler.User{
		User: &userCore,
	}
	reviewHandler := handler.Review{
		Review: &reviewCore,
	}
	commentHandler := handler.Comment{
		Comment: &commentCore,
	}

	// Add API V1 router
	apiV1Router := r.PathPrefix("/api/v1").Subrouter()

	// Initialize Routes

	// health routes
	healthRouter := apiV1Router.PathPrefix("/health").Subrouter()
	healthRouter.HandleFunc("", handlerHealth.HealthCheck).Methods(http.MethodGet)

	// docs routes
	docsRouter := apiV1Router.PathPrefix("/docs").Subrouter()
	docsRouter.HandleFunc("", docsHandler.GetDocsPage).Methods(http.MethodGet)
	docsRouter.HandleFunc("/swagger.yaml", docsHandler.GetSwaggerFile).Methods(http.MethodGet)

	// user routes
	userRouter := apiV1Router.PathPrefix("/users").Subrouter()
	userRouter.HandleFunc("", userHandler.InsertUser).Methods(http.MethodPost, http.MethodOptions)
	userRouter.HandleFunc("", userHandler.LoadUsers).Methods(http.MethodGet)
	userRouter.HandleFunc("/{userID}", userHandler.LoadUserByID).Methods(http.MethodGet)
	userRouter.HandleFunc("/{userID}", userHandler.RemoveUser).Methods(http.MethodDelete, http.MethodOptions)
	userRouter.HandleFunc("/{userID}/reviews", reviewHandler.LoadReviewByUserID).Methods(http.MethodGet)
	userRouter.HandleFunc("/{userID}/comments", commentHandler.LoadCommentByUserID).Methods(http.MethodGet)

	// review routes
	reviewRouter := apiV1Router.PathPrefix("/reviews").Subrouter()
	reviewRouter.HandleFunc("", reviewHandler.InsertReview).Methods(http.MethodPost, http.MethodOptions)
	reviewRouter.HandleFunc("", reviewHandler.LoadReviews).Methods(http.MethodGet)
	reviewRouter.HandleFunc("/{reviewID}", reviewHandler.LoadReviewByID).Methods(http.MethodGet)
	reviewRouter.HandleFunc("/{reviewID}", reviewHandler.RemoveReview).Methods(http.MethodDelete, http.MethodOptions)
	reviewRouter.HandleFunc("/{reviewID}/comments", commentHandler.LoadCommentByReviewID).Methods(http.MethodGet)

	// comments routes
	commentRouter := apiV1Router.PathPrefix("/comments").Subrouter()
	commentRouter.HandleFunc("", commentHandler.InsertComment).Methods(http.MethodPost, http.MethodOptions)
	commentRouter.HandleFunc("", commentHandler.LoadComments).Methods(http.MethodGet)
	commentRouter.HandleFunc("/{commentID}", commentHandler.LoadCommentByID).Methods(http.MethodGet)
	commentRouter.HandleFunc("/{commentID}", commentHandler.RemoveComment).Methods(http.MethodDelete, http.MethodOptions)
}

// Execute runs a HTTP server that contains the backend for the ea game review app
func Execute() {

	// initialize router
	router := mux.NewRouter()
	router.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
			w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
			if r.Method == http.MethodOptions {
				return
			}
			next.ServeHTTP(w, r)
		})
	})
	// load custom routes
	loadRoutes(router)

	// initialize http server configs
	server := http.Server{
		Addr:    fmt.Sprintf(":%s", config.BackendPort),
		Handler: router,
	}

	// start http server
	fmt.Printf("HTTP Server listening on port: %s\n", config.BackendPort)
	server.ListenAndServe()
}
