package core

import (
	"fmt"
	"strings"
)

var _ CoreUser = (*ServiceUser)(nil)

// ServiceUser contains the business logic in how it should handle, format, and return the
// payload or error
type ServiceUser struct {
	DatabaseUser DatabaseUser
}

// validate checks for the fields in the users object
func (u *User) validate() error {
	var sb strings.Builder

	if u.Username == "" {
		sb.WriteString("- missing Username\n")
	}
	if u.FirstName == "" {
		sb.WriteString("- missing First Name\n")
	}
	if u.LastName == "" {
		sb.WriteString("- missing Last Name\n")
	}

	if str := sb.String(); str != "" {
		return fmt.Errorf("%w: \n%s", ErrValidateUser, str)
	}
	return nil
}

// InsertUser inserts the user to the database given it's validity
func (su *ServiceUser) InsertUser(user *User) error {
	err := user.validate()
	if err != nil {
		return err
	}
	err = su.DatabaseUser.InsertUser(user)
	if err != nil {
		return fmt.Errorf("Error inserting user: %w", err)
	}
	return nil
}

// LoadUserByID inserts the user to the database given it's validity
func (su *ServiceUser) LoadUserByID(userID string) (*User, error) {
	userFilter := UserFilter{
		ID: userID,
	}
	users, err := su.DatabaseUser.LoadUser(userFilter)
	if err != nil {
		return nil, fmt.Errorf("Error loading user from the database: %w", err)
	}
	if users == nil {
		return nil, ErrUserNotFound
	}
	return &users[0], nil
}

// LoadUsers inserts the user to the database given it's validity
func (su *ServiceUser) LoadUsers() ([]User, error) {
	users, err := su.DatabaseUser.LoadUser(UserFilter{})
	if err != nil {
		return nil, fmt.Errorf("Error loading user from the database: %w", err)
	}
	if users == nil {
		return nil, nil
	}
	return users, nil
}

// RemoveUser inserts the user to the database given it's validity
func (su *ServiceUser) RemoveUser(userID string) error {
	if userID == "" {
		return ErrRemoveUserMissingUserID
	}
	err := su.DatabaseUser.RemoveUser(userID)
	if err != nil {
		return fmt.Errorf("Error removing the user from the database: %w", err)
	}
	return nil
}
