package core

import (
	"errors"
)

var (
	// ErrValidateUser should get thrown when a given users object is incomplete
	ErrValidateUser = errors.New("error validating users")
	// ErrRemoveUserMissingUserID should get thrown when a delete request comes in without
	// a user ID to be removed
	ErrRemoveUserMissingUserID = errors.New("user ID must be provided")
	// ErrUserNotFound should get thrown when desired user is not found in the database
	ErrUserNotFound = errors.New("error obtaining users")
)

// User contains the information needed to interact with a user
type User struct {
	ID        string `json:"id"`
	Username  string `json:"username"` // the username the user registers with
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

// CoreUser is an interface containing the business logic functions that may be used to
// manipulate the users entity
//go:generate mockgen -destination=./user_core.mock.go -self_package=gitlab.com/nicoalimin/ea-game-review/server/pkg/core -package core gitlab.com/nicoalimin/ea-game-review/server/pkg/core CoreUser
type CoreUser interface {
	// InsertUser validates that the given user is valid, and stores it in the desired
	// datastore. If a user is valid, it will replace the user
	InsertUser(user *User) error
	// LoadUserByID grabs the user from the desired datastore. If no user is found, it will
	// return nil, nil
	LoadUserByID(userID string) (*User, error)
	// LoadUsers grabs the user from the desired datastore. If no user is found, it will
	// return nil, nil
	LoadUsers() ([]User, error)
	// RemoveUser deletes the user from the desired datastore. If not user is found, it will
	// return nil
	RemoveUser(userID string) error
}

// UserFilter is used to filter the results from a given database implementation. If the
// filter is empty, it should return all users available
type UserFilter struct {
	ID string // The primary key of User, returns a single user by ID
}

// DatabaseUser is an interface to an implementation that does CRUD events to
// the user object
//go:generate mockgen -destination=./user_database.mock.go -self_package=gitlab.com/nicoalimin/ea-game-review/server/pkg/core -package core gitlab.com/nicoalimin/ea-game-review/server/pkg/core DatabaseUser
type DatabaseUser interface {
	// InsertUser persists a given user in the underlying store. If a user with
	// the same user ID already exists, it should replace the existing user
	InsertUser(*User) error
	// LoadUser returns the user for the given user ID. If no user is found, it
	// should return (nil slice, nil)
	LoadUser(userFilter UserFilter) ([]User, error)
	// RemoveUser removes a given user from the underlying store. If no user
	// exists with the given ID, it should return nil
	RemoveUser(userID string) error
}
