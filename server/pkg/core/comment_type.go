package core

import (
	"errors"
)

var (
	// ErrValidateComment should get thrown when a given comments object is incomplete
	ErrValidateComment = errors.New("error validating comments")
	// ErrRemoveCommentMissingCommentID should get thrown when a delete request comes in without
	// a comment ID to be removed
	ErrRemoveCommentMissingCommentID = errors.New("comment ID must be provided")
	// ErrCommentNotFound should get thrown when desired comment is not found in the database
	ErrCommentNotFound = errors.New("error obtaining comments")
)

// Comment contains the information needed to interact with a comment
type Comment struct {
	ID        string `json:"id"`
	Content   string `json:"content"`    // the string contents of the comment
	ReviewID  string `json:"review_id"`  // the review that this comment is associated to
	UserID    string `json:"user_id"`    // the creator of this comment
	CreatedAt string `json:"created_at"` // timestamp when the comment is created
}

// CoreComment is an interface containing the business logic functions that may be used to
// manipulate the comments entity
//go:generate mockgen -destination=./comment_core.mock.go -self_package=gitlab.com/nicoalimin/ea-game-review/server/pkg/core -package core gitlab.com/nicoalimin/ea-game-review/server/pkg/core CoreComment
type CoreComment interface {
	// InsertComment validates that the given comment is valid, and stores it in the desired
	// datastore. If a comment is valid, it will replace the comment
	InsertComment(comment *Comment) error
	// LoadCommentByID grabs the comment from the desired datastore. If no comment is found,
	// it will return nil, nil
	LoadCommentByID(commentID string) (*Comment, error)
	// LoadCommentByUserID grabs the comment from the desired datastore. If no comment is
	// found, it will return nil, nil
	LoadCommentByUserID(userID string) ([]Comment, error)
	// LoadCommentByReviewID grabs the comment from the desired datastore. If no comment is
	// found, it will return nil, nil
	LoadCommentByReviewID(reviewID string) ([]Comment, error)
	// LoadComments grabs the comment from the desired datastore. If no comment is found, it
	// will return nil, nil
	LoadComments() ([]Comment, error)
	// RemoveComment deletes the comment from the desired datastore. If not comment is
	// found, it will return nil
	RemoveComment(commentID string) error
}

// CommentFilter is to filter the results returned for any database implementation. Fields
// in this filter act as an AND query, where adding field members will limit the comments
// returned even further. If the filter is empty, all comments should be returned
type CommentFilter struct {
	ID       string // The primary key of the Comment, returns a single comment by ID
	ReviewID string // Filters the comments by a specific Review ID
	UserID   string // Filters the comments by User ID
}

// DatabaseComment is an interface to an implementation that does CRUD events to
// the comment object
//go:generate mockgen -destination=./comment_database.mock.go -self_package=gitlab.com/nicoalimin/ea-game-review/server/pkg/core -package core gitlab.com/nicoalimin/ea-game-review/server/pkg/core DatabaseComment
type DatabaseComment interface {
	// InsertComment persists a given comment in the underlying store. If a
	// comment with the same comment ID already exists, it should replace
	// the existing comment
	InsertComment(*Comment) error
	// LoadComment returns the comment for the given comment ID. If no comment
	// is found, it should return (nil, nil)
	LoadComment(commentFilter CommentFilter) ([]Comment, error)
	// RemoveComment removes a given comment from the underlying store. If no
	// comment exists with the given ID, it should return nil
	RemoveComment(commentID string) error
}
