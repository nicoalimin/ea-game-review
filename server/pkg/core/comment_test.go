package core

import (
	"errors"
	"testing"

	"github.com/go-test/deep"
	"github.com/golang/mock/gomock"
	"gitlab.com/nicoalimin/ea-game-review/server/pkg/utils"
)

// castCommentToInterfaceHelper is a helper function to case a concrete type Comment to an
// anonymous struct for easy comparison
func castCommentToInterfaceHelper(comments []Comment) []interface{} {
	interfaces := []interface{}{}
	for _, v := range comments {
		interfaces = append(interfaces, v)
	}
	return interfaces
}

func Test_InsertComment(t *testing.T) {

	cases := map[string]struct {
		comment                Comment
		numDatabaseCommentCall int
		numDatabaseReviewCall  int
		numDatabaseUserCall    int
		userExist              bool
		reviewExist            bool
		expectedErr            error
	}{
		"validCommentSuccess": {
			comment: Comment{
				Content:  "comment-content",
				ReviewID: "review-id",
				UserID:   "user-id",
			},
			numDatabaseCommentCall: 1,
			numDatabaseReviewCall:  1,
			numDatabaseUserCall:    1,
			userExist:              true,
			reviewExist:            true,
			expectedErr:            nil,
		},
		"invalidCommentUserNotExistFailure": {
			comment: Comment{
				Content:  "comment-content",
				ReviewID: "review-id",
				UserID:   "invalid-user",
			},
			numDatabaseUserCall:    1,
			numDatabaseReviewCall:  0,
			numDatabaseCommentCall: 0,
			userExist:              false,
			reviewExist:            false,
			expectedErr:            ErrUserNotFound,
		},
		"invalidCommentReviewNotExistFailure": {
			comment: Comment{
				Content:  "comment-content",
				ReviewID: "invalid-review",
				UserID:   "user-id",
			},
			numDatabaseUserCall:    1,
			numDatabaseReviewCall:  1,
			numDatabaseCommentCall: 0,
			userExist:              true,
			reviewExist:            false,
			expectedErr:            ErrReviewNotFound,
		},
		"invalidCommentMissingContentFailure": {
			comment: Comment{
				ReviewID: "review-id",
				UserID:   "user-id",
			},
			numDatabaseCommentCall: 0,
			numDatabaseReviewCall:  0,
			numDatabaseUserCall:    0,
			expectedErr:            ErrValidateComment,
		},
		"invalidCommentMissingReviewFailure": {
			comment: Comment{
				Content: "comment-content",
				UserID:  "user-id",
			},
			numDatabaseCommentCall: 0,
			numDatabaseReviewCall:  0,
			numDatabaseUserCall:    0,
			expectedErr:            ErrValidateComment,
		},
		"invalidCommentMissingUserFailure": {
			comment: Comment{
				Content:  "comment-content",
				ReviewID: "review-id",
			},
			numDatabaseCommentCall: 0,
			numDatabaseReviewCall:  0,
			numDatabaseUserCall:    0,
			expectedErr:            ErrValidateComment,
		},
		"invalidCommentFailure": {
			comment:                Comment{},
			numDatabaseCommentCall: 0,
			numDatabaseReviewCall:  0,
			numDatabaseUserCall:    0,
			expectedErr:            ErrValidateComment,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {

			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseComment := NewMockDatabaseComment(ctrl)
			mockDatabaseUser := NewMockDatabaseUser(ctrl)
			mockDatabaseReview := NewMockDatabaseReview(ctrl)
			coreComment := ServiceComment{
				DatabaseComment: mockDatabaseComment,
				DatabaseReview:  mockDatabaseReview,
				DatabaseUser:    mockDatabaseUser,
			}

			if c.userExist {
				mockDatabaseUser.EXPECT().LoadUser(UserFilter{ID: c.comment.UserID}).Times(c.numDatabaseUserCall).Return([]User{}, nil)
			} else {
				mockDatabaseUser.EXPECT().LoadUser(UserFilter{ID: c.comment.UserID}).Times(c.numDatabaseUserCall).Return(nil, nil)
			}
			if c.reviewExist {
				mockDatabaseReview.EXPECT().LoadReview(ReviewFilter{ID: c.comment.ReviewID}).Times(c.numDatabaseReviewCall).Return([]Review{}, nil)
			} else {
				mockDatabaseReview.EXPECT().LoadReview(ReviewFilter{ID: c.comment.ReviewID}).Times(c.numDatabaseReviewCall).Return(nil, nil)
			}
			mockDatabaseComment.EXPECT().InsertComment(&c.comment).Times(c.numDatabaseCommentCall).Return(nil)

			err := coreComment.InsertComment(&c.comment)
			if !errors.Is(err, c.expectedErr) {
				t.Errorf("expected no error when Inserting a valid comment. \nGot: %v", err)
			}
		})
	}
}

func Test_LoadCommentByID(t *testing.T) {

	commentID := "comment-id"

	cases := map[string]struct {
		desiredCommentDatabase []Comment
		desiredCommentReturned *Comment
		expectedError          error
	}{
		"validCommentSuccess": {
			desiredCommentDatabase: []Comment{{
				ID:      commentID,
				Content: "comment-content",
			}},
			desiredCommentReturned: &Comment{
				ID:      commentID,
				Content: "comment-content",
			},
			expectedError: nil,
		},
		"missingCommentDatabaseFailure": {
			desiredCommentDatabase: nil,
			desiredCommentReturned: nil,
			expectedError:          ErrCommentNotFound,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseComment := NewMockDatabaseComment(ctrl)
			coreComment := ServiceComment{
				DatabaseComment: mockDatabaseComment,
			}

			mockDatabaseComment.EXPECT().LoadComment(CommentFilter{ID: commentID}).Times(1).Return(c.desiredCommentDatabase, nil)

			actualComment, err := coreComment.LoadCommentByID(commentID)
			if !errors.Is(err, c.expectedError) {
				t.Errorf("actual error doesn't match expected. \nGot: %v \nWant: %v", err, c.expectedError)
			}
			if diff := deep.Equal(c.desiredCommentReturned, actualComment); len(diff) != 0 {
				t.Errorf("Expected comment doesn't match actual. \nGot: %v \nWant: %v", c.desiredCommentReturned, actualComment)
			}
		})
	}
}

func Test_LoadCommentByUserID(t *testing.T) {

	userID := "user-id"

	cases := map[string]struct {
		desiredCommentDatabase []Comment
		desiredCommentReturned []Comment
		expectedError          error
	}{
		"validCommentSuccess": {
			desiredCommentDatabase: []Comment{{
				ID:      "comment-id",
				UserID:  userID,
				Content: "comment-content",
			}, {
				ID:      "comment-id",
				UserID:  userID,
				Content: "comment-content-2",
			}},
			desiredCommentReturned: []Comment{{
				ID:      "comment-id",
				UserID:  userID,
				Content: "comment-content",
			}, {
				ID:      "comment-id",
				UserID:  userID,
				Content: "comment-content-2",
			}},
			expectedError: nil,
		},
		"missingCommentDatabaseFailure": {
			desiredCommentDatabase: nil,
			desiredCommentReturned: nil,
			expectedError:          ErrCommentNotFound,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseComment := NewMockDatabaseComment(ctrl)
			coreComment := ServiceComment{
				DatabaseComment: mockDatabaseComment,
			}

			mockDatabaseComment.EXPECT().LoadComment(CommentFilter{UserID: userID}).Times(1).Return(c.desiredCommentDatabase, nil)

			actualComment, err := coreComment.LoadCommentByUserID(userID)
			if !errors.Is(err, c.expectedError) {
				t.Errorf("actual error doesn't match expected. \nGot: %v \nWant: %v", err, c.expectedError)
			}
			if !utils.Equal(castCommentToInterfaceHelper(actualComment), castCommentToInterfaceHelper(c.desiredCommentReturned)) {
				t.Errorf("Expected comment doesn't match actual. \nGot: %v \nWant: %v", c.desiredCommentReturned, actualComment)
			}
		})
	}
}

func Test_LoadCommentByReviewID(t *testing.T) {

	reviewID := "review-id"

	cases := map[string]struct {
		desiredCommentDatabase []Comment
		desiredCommentReturned []Comment
		expectedError          error
	}{
		"validCommentSuccess": {
			desiredCommentDatabase: []Comment{{
				ID:       "comment-id",
				UserID:   "user-id",
				Content:  "comment-content",
				ReviewID: reviewID,
			}, {
				ID:       "comment-id",
				UserID:   "user-id",
				Content:  "comment-content-2",
				ReviewID: reviewID,
			}},
			desiredCommentReturned: []Comment{{
				ID:       "comment-id",
				UserID:   "user-id",
				Content:  "comment-content",
				ReviewID: reviewID,
			}, {
				ID:       "comment-id",
				UserID:   "user-id",
				Content:  "comment-content-2",
				ReviewID: reviewID,
			}},
			expectedError: nil,
		},
		"missingCommentDatabaseFailure": {
			desiredCommentDatabase: nil,
			desiredCommentReturned: nil,
			expectedError:          ErrCommentNotFound,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseComment := NewMockDatabaseComment(ctrl)
			coreComment := ServiceComment{
				DatabaseComment: mockDatabaseComment,
			}

			mockDatabaseComment.EXPECT().LoadComment(CommentFilter{ReviewID: reviewID}).Times(1).Return(c.desiredCommentDatabase, nil)

			actualComment, err := coreComment.LoadCommentByReviewID(reviewID)
			if !errors.Is(err, c.expectedError) {
				t.Errorf("actual error doesn't match expected. \nGot: %v \nWant: %v", err, c.expectedError)
			}
			if !utils.Equal(castCommentToInterfaceHelper(actualComment), castCommentToInterfaceHelper(c.desiredCommentReturned)) {
				t.Errorf("Expected comment doesn't match actual. \nGot: %v \nWant: %v", c.desiredCommentReturned, actualComment)
			}
		})
	}
}

func Test_LoadComments(t *testing.T) {

	cases := map[string]struct {
		desiredCommentDatabase []Comment
		expectedError          error
	}{
		"validCommentSuccess": {
			desiredCommentDatabase: []Comment{{
				ID:      "comment-id",
				Content: "comment-content",
			}},
			expectedError: nil,
		},
		"missingCommentDatabaseSuccess": {
			desiredCommentDatabase: nil,
			expectedError:          nil,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseComment := NewMockDatabaseComment(ctrl)
			coreComment := ServiceComment{
				DatabaseComment: mockDatabaseComment,
			}

			mockDatabaseComment.EXPECT().LoadComment(CommentFilter{}).Times(1).Return(c.desiredCommentDatabase, nil)

			actualComment, err := coreComment.LoadComments()
			if !errors.Is(err, c.expectedError) {
				t.Errorf("actual error doesn't match expected. \nGot: %v \nWant: %v", err, c.expectedError)
			}
			if diff := deep.Equal(c.desiredCommentDatabase, actualComment); len(diff) != 0 {
				t.Errorf("Expected comment doesn't match actual. \nGot: %v \nWant: %v", c.desiredCommentDatabase, actualComment)
			}
		})
	}
}

func Test_RemoveComment(t *testing.T) {

	cases := map[string]struct {
		commentIDRemove  string
		numDatabaseCalls int
		expectedError    error
	}{
		"validCommentSuccess": {
			commentIDRemove:  "comment-id",
			numDatabaseCalls: 1,
			expectedError:    nil,
		},
		"emptyCommentIDFailure": {
			commentIDRemove:  "",
			numDatabaseCalls: 0,
			expectedError:    ErrRemoveCommentMissingCommentID,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseComment := NewMockDatabaseComment(ctrl)
			coreComment := ServiceComment{
				DatabaseComment: mockDatabaseComment,
			}

			mockDatabaseComment.EXPECT().RemoveComment(c.commentIDRemove).Times(c.numDatabaseCalls).Return(nil)

			err := coreComment.RemoveComment(c.commentIDRemove)
			if !errors.Is(err, c.expectedError) {
				t.Errorf("actual error doesn't match expected. \nGot: %v \nWant: %v", err, c.expectedError)
			}
		})
	}
}
