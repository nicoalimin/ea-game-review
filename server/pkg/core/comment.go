package core

import (
	"fmt"
	"strings"
)

var _ CoreComment = (*ServiceComment)(nil)

// ServiceComment contains the business logic in how it should handle, format, and return the
// payload or error
type ServiceComment struct {
	DatabaseComment DatabaseComment
	DatabaseUser    DatabaseUser
	DatabaseReview  DatabaseReview
}

// validate checks for the fields in the comments object
func (r *Comment) validate() error {
	var sb strings.Builder

	if r.Content == "" {
		sb.WriteString("- missing Comment Content\n")
	}
	if r.ReviewID == "" {
		sb.WriteString("- missing Comment Review ID\n")
	}
	if r.UserID == "" {
		sb.WriteString("- missing Comment User ID\n")
	}

	if str := sb.String(); str != "" {
		return fmt.Errorf("%w: \n%s", ErrValidateComment, str)
	}
	return nil
}

// InsertComment inserts the comment to the database given it's validity
func (sc *ServiceComment) InsertComment(comment *Comment) error {
	err := comment.validate()
	if err != nil {
		return err
	}

	userFilter := UserFilter{
		ID: comment.UserID,
	}
	user, err := sc.DatabaseUser.LoadUser(userFilter)
	if err != nil {
		return fmt.Errorf("Failed to obtain user when inserting comment: %w", err)
	}
	if user == nil {
		return fmt.Errorf("Failed to obtain user when inserting comment: %w", ErrUserNotFound)
	}

	reviewFilter := ReviewFilter{
		ID: comment.ReviewID,
	}
	review, err := sc.DatabaseReview.LoadReview(reviewFilter)
	if err != nil {
		return fmt.Errorf("Failed to obtain review when inserting comment: %w", err)
	}
	if review == nil {
		return fmt.Errorf("Failed to obtain review when inserting comment: %w", ErrReviewNotFound)
	}

	err = sc.DatabaseComment.InsertComment(comment)
	if err != nil {
		return fmt.Errorf("Error inserting comment: %w", err)
	}
	return nil
}

// LoadCommentByID inserts the comment to the database given it's validity
func (sc *ServiceComment) LoadCommentByID(commentID string) (*Comment, error) {
	commentFilter := CommentFilter{
		ID: commentID,
	}
	comments, err := sc.DatabaseComment.LoadComment(commentFilter)
	if err != nil {
		return nil, fmt.Errorf("Error loading comment from the database: %w", err)
	}
	if comments == nil {
		return nil, ErrCommentNotFound
	}
	return &comments[0], nil
}

// LoadCommentByUserID inserts the comment to the database given it's validity
func (sc *ServiceComment) LoadCommentByUserID(userID string) ([]Comment, error) {
	commentFilter := CommentFilter{
		UserID: userID,
	}
	comments, err := sc.DatabaseComment.LoadComment(commentFilter)
	if err != nil {
		return nil, fmt.Errorf("Error loading comment from the database: %w", err)
	}
	if comments == nil {
		return nil, ErrCommentNotFound
	}
	return comments, nil
}

// LoadCommentByReviewID inserts the comment to the database given it's validity
func (sc *ServiceComment) LoadCommentByReviewID(reviewID string) ([]Comment, error) {
	commentFilter := CommentFilter{
		ReviewID: reviewID,
	}
	comments, err := sc.DatabaseComment.LoadComment(commentFilter)
	if err != nil {
		return nil, fmt.Errorf("Error loading comment from the database: %w", err)
	}
	if comments == nil {
		return nil, ErrCommentNotFound
	}
	return comments, nil
}

// LoadComments inserts the comment to the database given it's validity
func (sc *ServiceComment) LoadComments() ([]Comment, error) {
	comments, err := sc.DatabaseComment.LoadComment(CommentFilter{})
	if err != nil {
		return nil, fmt.Errorf("Error loading comment from the database: %w", err)
	}
	if comments == nil {
		return nil, nil
	}
	return comments, nil
}

// RemoveComment inserts the comment to the database given it's validity
func (sc *ServiceComment) RemoveComment(commentID string) error {
	if commentID == "" {
		return ErrRemoveCommentMissingCommentID
	}
	err := sc.DatabaseComment.RemoveComment(commentID)
	if err != nil {
		return fmt.Errorf("Error removing the comment from the database: %w", err)
	}
	return nil
}
