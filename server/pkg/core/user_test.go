package core

import (
	"errors"
	"testing"

	"github.com/go-test/deep"
	"github.com/golang/mock/gomock"
)

func Test_InsertUser(t *testing.T) {

	cases := map[string]struct {
		user            User
		numDatabaseCall int
		expectedErr     error
	}{
		"validUserSuccess": {
			user: User{
				Username:  "username",
				FirstName: "first_name",
				LastName:  "last_name",
			},
			numDatabaseCall: 1,
			expectedErr:     nil,
		},
		"invalidUserMissingUsernameFailure": {
			user: User{
				FirstName: "first_name",
				LastName:  "last_name",
			},
			numDatabaseCall: 0,
			expectedErr:     ErrValidateUser,
		},
		"invalidUserMissingFirstNameFailure": {
			user: User{
				Username: "username",
				LastName: "last_name",
			},
			numDatabaseCall: 0,
			expectedErr:     ErrValidateUser,
		},
		"invalidUserMissingLastNameFailure": {
			user: User{
				Username:  "username",
				FirstName: "first_name",
			},
			numDatabaseCall: 0,
			expectedErr:     ErrValidateUser,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {

			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseUser := NewMockDatabaseUser(ctrl)
			coreUser := ServiceUser{
				DatabaseUser: mockDatabaseUser,
			}

			mockDatabaseUser.EXPECT().InsertUser(&c.user).Times(c.numDatabaseCall).Return(nil)

			err := coreUser.InsertUser(&c.user)
			if !errors.Is(err, c.expectedErr) {
				t.Errorf("expected no error when Inserting a valid user. \nGot: %v", err)
			}
		})
	}
}

func Test_LoadUserByID(t *testing.T) {

	userID := "user-id"

	cases := map[string]struct {
		desiredUserDatabase []User
		desiredUserReturned *User
		expectedError       error
	}{
		"validUserSuccess": {
			desiredUserDatabase: []User{{
				ID:       userID,
				Username: "username",
			}},
			desiredUserReturned: &User{
				ID:       userID,
				Username: "username",
			},
			expectedError: nil,
		},
		"missingUserDatabaseFailure": {
			desiredUserDatabase: nil,
			desiredUserReturned: nil,
			expectedError:       ErrUserNotFound,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseUser := NewMockDatabaseUser(ctrl)
			coreUser := ServiceUser{
				DatabaseUser: mockDatabaseUser,
			}

			mockDatabaseUser.EXPECT().LoadUser(UserFilter{ID: userID}).Times(1).Return(c.desiredUserDatabase, nil)

			actualUser, err := coreUser.LoadUserByID(userID)
			if !errors.Is(err, c.expectedError) {
				t.Errorf("actual error doesn't match expected. \nGot: %v \nWant: %v", err, c.expectedError)
			}
			if diff := deep.Equal(c.desiredUserReturned, actualUser); len(diff) != 0 {
				t.Errorf("Expected user doesn't match actual. \nGot: %v \nWant: %v", c.desiredUserReturned, actualUser)
			}
		})
	}
}

func Test_LoadUsers(t *testing.T) {

	cases := map[string]struct {
		desiredUserDatabase []User
		expectedError       error
	}{
		"validUserSuccess": {
			desiredUserDatabase: []User{{
				ID:       "user-id",
				Username: "username",
			}},
			expectedError: nil,
		},
		"missingUsersSuccess": {
			desiredUserDatabase: nil,
			expectedError:       nil,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseUser := NewMockDatabaseUser(ctrl)
			coreUser := ServiceUser{
				DatabaseUser: mockDatabaseUser,
			}

			mockDatabaseUser.EXPECT().LoadUser(UserFilter{}).Times(1).Return(c.desiredUserDatabase, nil)

			actualUser, err := coreUser.LoadUsers()
			if !errors.Is(err, c.expectedError) {
				t.Errorf("actual error doesn't match expected. \nGot: %v \nWant: %v", err, c.expectedError)
			}
			if diff := deep.Equal(c.desiredUserDatabase, actualUser); len(diff) != 0 {
				t.Errorf("Expected user doesn't match actual. \nGot: %v \nWant: %v", c.desiredUserDatabase, actualUser)
			}
		})
	}
}

func Test_RemoveUser(t *testing.T) {

	cases := map[string]struct {
		userIDRemove     string
		numDatabaseCalls int
		expectedError    error
	}{
		"validUserSuccess": {
			userIDRemove:     "user-id",
			numDatabaseCalls: 1,
			expectedError:    nil,
		},
		"emptyUserIDFailure": {
			userIDRemove:     "",
			numDatabaseCalls: 0,
			expectedError:    ErrRemoveUserMissingUserID,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseUser := NewMockDatabaseUser(ctrl)
			coreUser := ServiceUser{
				DatabaseUser: mockDatabaseUser,
			}

			mockDatabaseUser.EXPECT().RemoveUser(c.userIDRemove).Times(c.numDatabaseCalls).Return(nil)

			err := coreUser.RemoveUser(c.userIDRemove)
			if !errors.Is(err, c.expectedError) {
				t.Errorf("actual error doesn't match expected. \nGot: %v \nWant: %v", err, c.expectedError)
			}
		})
	}
}
