package core

import (
	"errors"
	"testing"

	"github.com/go-test/deep"
	"github.com/golang/mock/gomock"
	"gitlab.com/nicoalimin/ea-game-review/server/pkg/utils"
)

// castReviewToInterfaceHelper is a helper function to case a concrete type Review to an
// anonymous struct for easy comparison
func castReviewToInterfaceHelper(reviews []Review) []interface{} {
	interfaces := []interface{}{}
	for _, v := range reviews {
		interfaces = append(interfaces, v)
	}
	return interfaces
}

func Test_InsertReview(t *testing.T) {
	cases := map[string]struct {
		review                Review
		numDatabaseReviewCall int
		numDatabaseUserCall   int
		userExist             bool
		expectedErr           error
	}{
		"validReviewSuccess": {
			review: Review{
				Content: "review-content",
				Title:   "review-title",
				UserID:  "user-id",
			},
			numDatabaseReviewCall: 1,
			numDatabaseUserCall:   1,
			userExist:             true,
			expectedErr:           nil,
		},
		"validReviewMissingUserFailure": {
			review: Review{
				Content: "review-content",
				Title:   "review-title",
				UserID:  "user-id",
			},
			numDatabaseReviewCall: 0,
			numDatabaseUserCall:   1,
			userExist:             false,
			expectedErr:           ErrUserNotFound,
		},
		"invalidReviewMissingContentFailure": {
			review: Review{
				Title:  "review-title",
				UserID: "user-id",
			},
			numDatabaseReviewCall: 0,
			numDatabaseUserCall:   0,
			expectedErr:           ErrValidateReview,
		},
		"invalidReviewMissingTitleFailure": {
			review: Review{
				Content: "review-content",
				UserID:  "user-id",
			},
			numDatabaseReviewCall: 0,
			numDatabaseUserCall:   0,
			expectedErr:           ErrValidateReview,
		},
		"invalidReviewMissingUserFailure": {
			review: Review{
				Content: "review-content",
				Title:   "review-title",
			},
			numDatabaseReviewCall: 0,
			numDatabaseUserCall:   0,
			expectedErr:           ErrValidateReview,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseReview := NewMockDatabaseReview(ctrl)
			mockDatabaseUser := NewMockDatabaseUser(ctrl)
			coreReview := ServiceReview{
				DatabaseReview: mockDatabaseReview,
				DatabaseUser:   mockDatabaseUser,
			}

			if c.userExist {
				mockDatabaseUser.EXPECT().LoadUser(UserFilter{ID: c.review.UserID}).Times(c.numDatabaseUserCall).Return([]User{}, nil)
			} else {
				mockDatabaseUser.EXPECT().LoadUser(UserFilter{ID: c.review.UserID}).Times(c.numDatabaseUserCall).Return(nil, nil)
			}
			mockDatabaseReview.EXPECT().InsertReview(&c.review).Times(c.numDatabaseReviewCall).Return(nil)

			err := coreReview.InsertReview(&c.review)
			if !errors.Is(err, c.expectedErr) {
				t.Errorf("actual error does not match expected. \nGot: %v, \nWant: %v", err, c.expectedErr)
			}
		})
	}
}

func Test_LoadReviewByID(t *testing.T) {
	reviewID := "review-id"

	cases := map[string]struct {
		desiredReviewDatabase []Review
		desiredReviewReturned *Review
		expectedError         error
	}{
		"validReviewSuccess": {
			desiredReviewDatabase: []Review{{
				ID:      reviewID,
				Content: "review-content",
			}},
			desiredReviewReturned: &Review{
				ID:      reviewID,
				Content: "review-content",
			},
			expectedError: nil,
		},
		"missingReviewDatabaseFailure": {
			desiredReviewDatabase: nil,
			desiredReviewReturned: nil,
			expectedError:         ErrReviewNotFound,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseReview := NewMockDatabaseReview(ctrl)
			coreReview := ServiceReview{
				DatabaseReview: mockDatabaseReview,
			}

			mockDatabaseReview.EXPECT().LoadReview(ReviewFilter{ID: reviewID}).Times(1).Return(c.desiredReviewDatabase, nil)

			actualReview, err := coreReview.LoadReviewByID(reviewID)
			if !errors.Is(err, c.expectedError) {
				t.Errorf("actual error doesn't match expected. \nGot: %v \nWant: %v", err, c.expectedError)
			}
			if diff := deep.Equal(c.desiredReviewReturned, actualReview); len(diff) != 0 {
				t.Errorf("Expected review doesn't match actual. \nGot: %v \nWant: %v", c.desiredReviewReturned, actualReview)
			}
		})
	}
}

func Test_LoadReviewByUserID(t *testing.T) {
	userID := "user-id"

	cases := map[string]struct {
		desiredReviewDatabase []Review
		desiredReviewReturned []Review
		expectedError         error
	}{
		"validReviewSuccess": {
			desiredReviewDatabase: []Review{{
				ID:      "review-id",
				UserID:  userID,
				Content: "review-content",
			}, {
				ID:      "review-id",
				UserID:  userID,
				Content: "review-content-2",
			}},
			desiredReviewReturned: []Review{{
				ID:      "review-id",
				UserID:  userID,
				Content: "review-content",
			}, {
				ID:      "review-id",
				UserID:  userID,
				Content: "review-content-2",
			}},
			expectedError: nil,
		},
		"missingReviewDatabaseFailure": {
			desiredReviewDatabase: nil,
			desiredReviewReturned: nil,
			expectedError:         ErrReviewNotFound,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseReview := NewMockDatabaseReview(ctrl)
			coreReview := ServiceReview{
				DatabaseReview: mockDatabaseReview,
			}

			mockDatabaseReview.EXPECT().LoadReview(ReviewFilter{UserID: userID}).Times(1).Return(c.desiredReviewDatabase, nil)

			actualReview, err := coreReview.LoadReviewByUserID(userID)
			if !errors.Is(err, c.expectedError) {
				t.Errorf("actual error doesn't match expected. \nGot: %v \nWant: %v", err, c.expectedError)
			}
			if !utils.Equal(castReviewToInterfaceHelper(actualReview), castReviewToInterfaceHelper(c.desiredReviewReturned)) {
				t.Errorf("Expected review doesn't match actual. \nGot: %v \nWant: %v", c.desiredReviewReturned, actualReview)
			}
		})
	}
}

func Test_LoadReviews(t *testing.T) {
	cases := map[string]struct {
		desiredReviewDatabase []Review
		expectedError         error
	}{
		"validReviewSuccess": {
			desiredReviewDatabase: []Review{{
				ID:      "review-id",
				Content: "review-content",
			}},
			expectedError: nil,
		},
		"missingReviewsSuccess": {
			desiredReviewDatabase: nil,
			expectedError:         nil,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseReview := NewMockDatabaseReview(ctrl)
			coreReview := ServiceReview{
				DatabaseReview: mockDatabaseReview,
			}

			mockDatabaseReview.EXPECT().LoadReview(ReviewFilter{}).Times(1).Return(c.desiredReviewDatabase, nil)

			actualReview, err := coreReview.LoadReviews()
			if !errors.Is(err, c.expectedError) {
				t.Errorf("actual error doesn't match expected. \nGot: %v \nWant: %v", err, c.expectedError)
			}
			if diff := deep.Equal(c.desiredReviewDatabase, actualReview); len(diff) != 0 {
				t.Errorf("Expected review doesn't match actual. \nGot: %v \nWant: %v", c.desiredReviewDatabase, actualReview)
			}
		})
	}
}

func Test_RemoveReview(t *testing.T) {
	cases := map[string]struct {
		reviewIDRemove   string
		numDatabaseCalls int
		expectedError    error
	}{
		"validReviewSuccess": {
			reviewIDRemove:   "review-id",
			numDatabaseCalls: 1,
			expectedError:    nil,
		},
		"emptyReviewIDFailure": {
			reviewIDRemove:   "",
			numDatabaseCalls: 0,
			expectedError:    ErrRemoveReviewMissingReviewID,
		},
	}

	for n, c := range cases {
		t.Run(n, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockDatabaseReview := NewMockDatabaseReview(ctrl)
			coreReview := ServiceReview{
				DatabaseReview: mockDatabaseReview,
			}

			mockDatabaseReview.EXPECT().RemoveReview(c.reviewIDRemove).Times(c.numDatabaseCalls).Return(nil)

			err := coreReview.RemoveReview(c.reviewIDRemove)
			if !errors.Is(err, c.expectedError) {
				t.Errorf("actual error doesn't match expected. \nGot: %v \nWant: %v", err, c.expectedError)
			}
		})
	}
}
