package core

import (
	"fmt"
	"strings"
)

var _ CoreReview = (*ServiceReview)(nil)

// ServiceReview contains the business logic in how it should handle, format, and return the
// payload or error
type ServiceReview struct {
	DatabaseReview DatabaseReview
	DatabaseUser   DatabaseUser
}

// validate checks for the fields in the reviews object
func (r *Review) validate() error {
	var sb strings.Builder

	if r.Title == "" {
		sb.WriteString("- missing Review Title\n")
	}
	if r.Content == "" {
		sb.WriteString("- missing Review Content\n")
	}
	if r.UserID == "" {
		sb.WriteString("- missing Review User ID\n")
	}

	if str := sb.String(); str != "" {
		return fmt.Errorf("%w: \n%s", ErrValidateReview, str)
	}
	return nil
}

// InsertReview inserts the review to the database given it's validity
func (su *ServiceReview) InsertReview(review *Review) error {
	err := review.validate()
	if err != nil {
		return err
	}

	userFilter := UserFilter{
		ID: review.UserID,
	}
	user, err := su.DatabaseUser.LoadUser(userFilter)
	if err != nil {
		return fmt.Errorf("Failed to obtain user when inserting review: %w", err)
	}
	if user == nil {
		return fmt.Errorf("Failed to obtain user when inserting review: %w", ErrUserNotFound)
	}

	err = su.DatabaseReview.InsertReview(review)
	if err != nil {
		return fmt.Errorf("Error inserting review: %w", err)
	}
	return nil
}

// LoadReviewByID inserts the review to the database given it's validity
func (su *ServiceReview) LoadReviewByID(reviewID string) (*Review, error) {
	reviewFilter := ReviewFilter{
		ID: reviewID,
	}
	reviews, err := su.DatabaseReview.LoadReview(reviewFilter)
	if err != nil {
		return nil, fmt.Errorf("Error loading review from the database: %w", err)
	}
	if reviews == nil {
		return nil, ErrReviewNotFound
	}
	return &reviews[0], nil
}

// LoadReviewByUserID inserts the review to the database given it's validity
func (su *ServiceReview) LoadReviewByUserID(userID string) ([]Review, error) {
	reviewFilter := ReviewFilter{
		UserID: userID,
	}
	reviews, err := su.DatabaseReview.LoadReview(reviewFilter)
	if err != nil {
		return nil, fmt.Errorf("Error loading review from the database: %w", err)
	}
	if reviews == nil {
		return nil, ErrReviewNotFound
	}
	return reviews, nil
}

// LoadReviews inserts the review to the database given it's validity
func (su *ServiceReview) LoadReviews() ([]Review, error) {
	reviews, err := su.DatabaseReview.LoadReview(ReviewFilter{})
	if err != nil {
		return nil, fmt.Errorf("Error loading review from the database: %w", err)
	}
	if reviews == nil {
		return nil, nil
	}
	return reviews, nil
}

// RemoveReview inserts the review to the database given it's validity
func (su *ServiceReview) RemoveReview(reviewID string) error {
	if reviewID == "" {
		return ErrRemoveReviewMissingReviewID
	}
	err := su.DatabaseReview.RemoveReview(reviewID)
	if err != nil {
		return fmt.Errorf("Error removing the review from the database: %w", err)
	}
	return nil
}
