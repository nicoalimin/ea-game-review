package core

import (
	"errors"
)

var (
	// ErrValidateReview should get thrown when a given reviews object is incomplete
	ErrValidateReview = errors.New("error validating reviews")
	// ErrRemoveReviewMissingReviewID should get thrown when a delete request comes in without
	// a review ID to be removed
	ErrRemoveReviewMissingReviewID = errors.New("review ID must be provided")
	// ErrReviewNotFound should get thrown when desired review is not found in the database
	ErrReviewNotFound = errors.New("error obtaining reviews")
)

// Review contains the information needed to interact with a review
type Review struct {
	ID        string `json:"id"`
	Title     string `json:"title"`      // the review title
	Content   string `json:"content"`    // contains the string review data
	UserID    string `json:"user_id"`    // the creator of the review
	CreatedAt string `json:"created_at"` // timestamp when the review is created
}

// CoreReview is an interface containing the business logic functions that may be used to
// manipulate the reviews entity
//go:generate mockgen -destination=./review_core.mock.go -self_package=gitlab.com/nicoalimin/ea-game-review/server/pkg/core -package core gitlab.com/nicoalimin/ea-game-review/server/pkg/core CoreReview
type CoreReview interface {
	// InsertReview validates that the given review is valid, and stores it in the desired
	// datastore. If a review is valid, it will replace the review
	InsertReview(review *Review) error
	// LoadReviewByID grabs the review from the desired datastore. If no review is found, it will
	// return nil, nil
	LoadReviewByID(reviewID string) (*Review, error)
	// LoadReviewByUserID grabs the review from the desired datastore. If no review is found, it will
	// return nil, nil
	LoadReviewByUserID(userID string) ([]Review, error)
	// LoadReviews grabs the review from the desired datastore. If no review is found, it will
	// return nil, nil
	LoadReviews() ([]Review, error)
	// RemoveReview deletes the review from the desired datastore. If not review is found, it will
	// return nil
	RemoveReview(reviewID string) error
}

// ReviewFilter is to filter the results returned for any database implementation. Fields
// in this filter act as an AND query, where adding field members will limit the reviews
// returned even further. If the filter is empty, all reviews should be returned
type ReviewFilter struct {
	ID     string // The primary key of the Review, returns a single Review by ID
	UserID string // Filters all reviews by a User ID
}

// DatabaseReview is an interface to an implementation that does CRUD events to
// the review object
//go:generate mockgen -destination=./review_database.mock.go -self_package=gitlab.com/nicoalimin/ea-game-review/server/pkg/core -package core gitlab.com/nicoalimin/ea-game-review/server/pkg/core DatabaseReview
type DatabaseReview interface {
	// InsertReview persists a given review in the underlying store. If a review
	// with the same review ID already exists, it should replace the existing review
	InsertReview(*Review) error
	// LoadReview returns the review for the given review ID. If no review is
	// found, it should return (nil, nil)
	LoadReview(reviewFilter ReviewFilter) ([]Review, error)
	// RemoveReview removes a given review from the underlying store. If no
	// review exists with the given ID, it should return nil
	RemoveReview(reviewID string) error
}
