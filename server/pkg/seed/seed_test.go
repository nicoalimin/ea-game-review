package seed

import "testing"

func Test_UserFactory(t *testing.T) {
	userFactory := userFactory{iDCounter: 0}
	user := userFactory.generateUser("username", "firstname", "lastname")
	if user == nil {
		t.Fatalf("expected factory to generate a user successfully, got nil")
	}
}

func Test_ReviewFactory(t *testing.T) {
	reviewFactory := reviewFactory{iDCounter: 0}
	review := reviewFactory.generateReview("title", "content", "user-id")
	if review == nil {
		t.Fatalf("expected factory to generate a review successfully, got nil")
	}
}

func Test_CommentFactory(t *testing.T) {
	commentFactory := commentFactory{iDCounter: 0}
	comment := commentFactory.generateComment("content", "review-id", "user-id")
	if comment == nil {
		t.Fatalf("expected factory to generate a comment successfully, got nil")
	}
}

func Test_GenerateSeed(t *testing.T) {
	users, reviews, comments := GenerateSeed()

	for _, v := range users {
		if v.ID == "" {
			t.Fatalf("expected valid user to be generated, \nGot: %v", v)
		}
	}

	for _, v := range reviews {
		if v.ID == "" {
			t.Fatalf("expected valid review to be generated, \nGot: %v", v)
		}
	}

	for _, v := range comments {
		if v.ID == "" {
			t.Fatalf("expected valid comment to be generated, \nGot: %v", v)
		}
	}
}
