package seed

import (
	"strconv"
	"time"

	"gitlab.com/nicoalimin/ea-game-review/server/pkg/core"
)

type userFactory struct {
	iDCounter int
}

func (uf *userFactory) generateUser(username, firstname, lastname string) *core.User {
	user := core.User{
		ID:        strconv.Itoa(uf.iDCounter),
		Username:  username,
		FirstName: firstname,
		LastName:  lastname,
	}
	uf.iDCounter = uf.iDCounter + 1
	return &user
}

type reviewFactory struct {
	iDCounter int
}

func (rf *reviewFactory) generateReview(title, content, userID string) *core.Review {
	review := core.Review{
		ID:        strconv.Itoa(rf.iDCounter),
		Title:     title,
		Content:   content,
		UserID:    userID,
		CreatedAt: time.Now().UTC().Format("2006-01-02T15:04:05-0700"),
	}
	rf.iDCounter = rf.iDCounter + 1
	return &review
}

type commentFactory struct {
	iDCounter int
}

func (cf *commentFactory) generateComment(content, reviewID, userID string) *core.Comment {
	comment := core.Comment{
		ID:        strconv.Itoa(cf.iDCounter),
		Content:   content,
		ReviewID:  reviewID,
		UserID:    userID,
		CreatedAt: time.Now().UTC().Format("2006-01-02T15:04:05-0700"),
	}
	cf.iDCounter = cf.iDCounter + 1
	return &comment
}

// GenerateSeed returns the seeded users, reviews, and comments
func GenerateSeed() (users map[string]core.User, reviews map[string]core.Review, comments map[string]core.Comment) {
	uf := userFactory{iDCounter: 1}
	rf := reviewFactory{iDCounter: 1}
	cf := commentFactory{iDCounter: 1}

	u1 := uf.generateUser("nicoalimin", "Nico", "Alimin")
	u2 := uf.generateUser("kirbyburchill", "Kirby", "Burchill")
	u3 := uf.generateUser("justintrudeau", "Justin", "Trudeau")
	u4 := uf.generateUser("tonystark", "Tony", "Stark")

	r1 := rf.generateReview(
		"FIFA 20 is amazing",
		"Summary: Powered by Frostbite, EA Sports FIFA 20 brings two sides of The World’s Game to life -- the prestige of the professional stage and an all-new authentic street football experience in EA Sports Volta. FIFA 20 innovates across the game, Football Intelligence unlocks an unprecedented platform for gameplay realism, FIFA Ultimate Team offers more ways to build your dream squad, and EA Sports Volta returns the game to the street, with an authentic form of small-sided football. (From IGN)",
		u1.ID,
	)
	r2 := rf.generateReview(
		"Sims 4",
		"Over the past week I’ve become intimately familiar with the virtual life of a geeky tech-industry maven named Jess, who I gave wide, swishy hips and a sly smirk. As of this writing, she has completed the level 10 programming skill, felt sad once and happy 232 times, WooHooed only nine times, and had two kids with the husband she met at a bar. I’m having a hard time saying goodbye to her, admittedly, so I have her drinking anti-aging potions. I encounter this problem with a lot of Sims games, and this time it’s even worse (because a lot of things are better). But at the same time, The Sims 4’s lack of features and small world hasn’t let Jess live her life to the fullest. Even though it’s fun, it’s a big letdown that this sequel doesn’t inspire the same wow factor that The Sims 3 did when I first played it years ago. (From IGN)",
		u3.ID,
	)
	r3 := rf.generateReview(
		"Star Wars Battlefront",
		"I can do better. Star Wars Battlefront II is an action shooter video game based on the Star Wars film franchise. It is the fourth major installment of the Star Wars: Battlefront series and seventh overall, and a sequel to the 2015 reboot of the series",
		u4.ID,
	)

	c1 := cf.generateComment(
		"I wish the whole Piemonte Calcio can be worked out",
		r1.ID,
		u3.ID,
	)
	c2 := cf.generateComment(
		"Yeah...",
		r1.ID,
		u2.ID,
	)
	c3 := cf.generateComment(
		"I prefer Sims 3 more",
		r2.ID,
		u1.ID,
	)
	c4 := cf.generateComment(
		"I can do even better. Star Wars Battlefront II is an action shooter video game based on the Star Wars film franchise. It is the fourth major installment of the Star Wars: Battlefront series and seventh overall, and a sequel to the 2015 reboot of the series",
		r3.ID,
		u3.ID,
	)

	users = map[string]core.User{u1.ID: (*u1), u2.ID: (*u2), u3.ID: (*u3), u4.ID: (*u4)}
	reviews = map[string]core.Review{r1.ID: (*r1), r2.ID: (*r2), r3.ID: (*r3)}
	comments = map[string]core.Comment{c1.ID: (*c1), c2.ID: (*c2), c3.ID: (*c3), c4.ID: (*c4)}
	return
}
