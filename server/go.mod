module gitlab.com/nicoalimin/ea-game-review/server

go 1.13

require (
	github.com/go-test/deep v1.0.5
	github.com/golang/mock v1.4.3
	github.com/gorilla/mux v1.7.4
	github.com/kelseyhightower/envconfig v1.4.0
)
