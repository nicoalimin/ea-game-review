package main

import (
	server "gitlab.com/nicoalimin/ea-game-review/server/pkg"
)

func main() {
	server.Execute()
}

// TODO: Polishing
// TODO: Create e2e tests
// TODO: Add badges to readme
// TODO: Add godoc
