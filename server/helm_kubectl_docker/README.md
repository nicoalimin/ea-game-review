# Helm Kubectl Docker Image

This is an image that is used to upgrade the Helm deployment.
This image is bundled up and published to minimize load on the CI step
Otherwise, the CI step would need to download the following:

- Kubectl
- Helm
- AWS CLI
