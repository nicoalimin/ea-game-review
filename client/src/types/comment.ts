export type Comment = {
  id: string;
  content: string;
  review_id: string;
  user_id: string;
  created_at: string;
};
