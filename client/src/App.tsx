import React from "react";
import { Header } from "./components/Header";
import { Reviews } from "./components/Reviews";

function App() {
  return (
    <div
      className="App"
      style={{
        background: "#F7F9FC",
        height: "100%",
        width: "100%"
      }}
    >
      <header
        className="App-header"
        style={{ minHeight: "unset", backgroundColor: "unset" }}
      >
        <Header />
      </header>
      <Reviews />
    </div>
  );
}

export default App;
