import React, { PureComponent } from "react";
import { Heading, Pane } from "evergreen-ui";
import logo from "../static/ea_logo.png";

export class Header extends PureComponent {
  render() {
    return (
      <Pane width="100%" display="flex" height="80px">
        <Pane marginTop="14px" marginLeft="20px" position="absolute">
          <img src={logo} alt="logo" height="50px" />
        </Pane>
        <Heading color="#121F4E" size={700} margin="auto">
          EA Game Review Blog
        </Heading>
      </Pane>
    );
  }
}
