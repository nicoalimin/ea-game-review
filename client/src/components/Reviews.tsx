import React, { Component } from "react";
import { Pane, Spinner, Textarea } from "evergreen-ui";
import axios from "axios";
import { Review } from "./Review";
import { Review as ReviewType } from "../types";
import { WriteReview } from "./WriteReview";

interface ReviewsState {
  reviews: ReviewType[];
}

interface ReviewsProps {}

export class Reviews extends Component<ReviewsProps, ReviewsState> {
  constructor(props: ReviewsProps) {
    super(props);
    this.state = {
      reviews: []
    };
  }

  async fetchReviews() {
    try {
      const reviews = await axios.get(
        `${process.env.REACT_APP_BACKEND_URL}/reviews`
      );
      this.setState({ reviews: reviews?.data?.data });
    } catch (err) {
      console.error("Cannot reach the backend");
    }
  }

  async componentDidMount() {
    await this.fetchReviews();
  }

  render() {
    if (!this?.state?.reviews?.length) {
      return (
        <div>
          <Spinner />
        </div>
      );
    }
    const reviews = this.state.reviews.map(review => {
      return <Review review={review} key={review.id} />;
    });
    return (
      <Pane
        marginLeft="5%"
        width="90%"
        display="flex"
        flexDirection="column"
        alignItems="center"
      >
        {reviews}
        <WriteReview refreshPosts={() => this.fetchReviews()} />
      </Pane>
    );
  }
}
