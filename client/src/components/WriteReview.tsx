import React, { Component } from "react";
import {
  Pane,
  Textarea,
  TextInputField,
  Label,
  Button,
  Dialog,
  Heading
} from "evergreen-ui";
import Axios from "axios";

interface WriteReviewProps {
  refreshPosts: () => void;
}

interface WriteReviewState {
  title: string;
  content: string;
  showDialog: boolean;
  username: string;
  first_name: string;
  last_name: string;
}

const initialState: WriteReviewState = {
  title: "",
  content: "",
  showDialog: false,
  username: "",
  first_name: "",
  last_name: ""
};

export class WriteReview extends Component<WriteReviewProps, WriteReviewState> {
  constructor(props: WriteReviewProps) {
    super(props);
    this.state = initialState;
  }

  isConfirmSubmissionDisabled() {
    return (
      this.state.username === "" ||
      this.state.first_name === "" ||
      this.state.last_name === ""
    );
  }

  isSubmitDisabled() {
    return this.state.title === "" || this.state.content === "";
  }

  resetState() {
    this.setState(initialState);
  }

  async submitReview() {
    try {
      const res = await Axios.post(
        `${process.env.REACT_APP_BACKEND_URL}/users`,
        {
          username: this.state.username,
          first_name: this.state.first_name,
          last_name: this.state.last_name
        }
      );
      const userID = res?.data?.data;
      if (userID) {
        await Axios.post(`${process.env.REACT_APP_BACKEND_URL}/reviews`, {
          title: this.state.title,
          content: this.state.content,
          user_id: userID
        });
      }
    } catch (err) {
      console.error(err);
    }
    this.props.refreshPosts();
    this.resetState();
  }

  render() {
    return (
      <Pane
        maxWidth="1000px"
        width="90%"
        padding="20px"
        margin="30px"
        borderRadius={10}
        boxShadow="0px 4px 6px rgba(0, 0, 0, 0.2)"
        display="flex"
        flexDirection="column"
      >
        <Pane width="100%">
          <TextInputField
            label="Title"
            placeholder="Review Title"
            onChange={(e: { target: { value: any } }) =>
              this.setState({ title: e.target.value })
            }
            value={this.state.title}
          />
        </Pane>
        <Pane width="100%" marginBottom={4}>
          <Label marginBottom={4} display="block">
            Review
          </Label>
          <Textarea
            placeholder="Write your review here..."
            onChange={(e: { target: { value: any } }) =>
              this.setState({ content: e.target.value })
            }
            value={this.state.content}
          />
        </Pane>
        <Pane display="flex" flexFlow="row-reverse" width="100%">
          <Button
            appearance="primary"
            intent="success"
            onClick={() => this.setState({ showDialog: true })}
            disabled={this.isSubmitDisabled()}
          >
            Submit
          </Button>
        </Pane>
        <Pane>
          <Dialog
            isShown={this.state.showDialog}
            title="Confirm Submission"
            onCloseComplete={() => this.setState({ showDialog: false })}
            onConfirm={() => this.submitReview()}
            isConfirmDisabled={this.isConfirmSubmissionDisabled()}
          >
            <Pane>
              <Heading marginBottom="5px">Title: {this.state.title}</Heading>
              <Heading marginBottom="10px">
                Content: {this.state.content}
              </Heading>
            </Pane>
            <Pane display="flex">
              <Pane width="33%">
                <TextInputField
                  width="80%"
                  marginBottom="5px"
                  label="Username"
                  placeholder="Insert username..."
                  onChange={(e: { target: { value: any } }) =>
                    this.setState({ username: e.target.value })
                  }
                  value={this.state.username}
                />
              </Pane>
              <Pane width="33%">
                <TextInputField
                  width="80%"
                  marginBottom="5px"
                  label="First Name"
                  placeholder="Insert First Name..."
                  onChange={(e: { target: { value: any } }) =>
                    this.setState({ first_name: e.target.value })
                  }
                  value={this.state.first_name}
                />
              </Pane>
              <Pane width="33%">
                <TextInputField
                  width="80%"
                  marginBottom="5px"
                  label="Last Name"
                  placeholder="Insert Last Name..."
                  onChange={(e: { target: { value: any } }) =>
                    this.setState({ last_name: e.target.value })
                  }
                  value={this.state.last_name}
                />
              </Pane>
            </Pane>
          </Dialog>
        </Pane>
      </Pane>
    );
  }
}
