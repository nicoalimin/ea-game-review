import React, { Component } from "react";
import { Pane, Heading } from "evergreen-ui";
import { Review as ReviewType, User as UserType } from "../types";
import axios from "axios";

interface ReviewProps {
  review: ReviewType;
}

interface ReviewState {
  creator: UserType;
}

export class Review extends Component<ReviewProps, ReviewState> {
  constructor(props: ReviewProps) {
    super(props);
    this.state = {
      creator: {
        id: "",
        username: "",
        first_name: "",
        last_name: ""
      }
    };
  }

  async componentDidMount() {
    try {
      const res = await axios.get(
        `${process.env.REACT_APP_BACKEND_URL}/users/${this.props.review.user_id}`
      );
      if (res?.data?.data) {
        this.setState({ creator: res.data.data });
      }
    } catch (err) {
      console.error(err);
    }
  }

  render() {
    return (
      <Pane
        maxWidth="1000px"
        width="90%"
        padding="20px"
        margin="30px"
        borderRadius={10}
        boxShadow="0px 4px 6px rgba(0, 0, 0, 0.2)"
        display="flex"
        flexDirection="row"
      >
        <Pane flexDirection="column" width="40%">
          <Pane width="50%">
            <Heading color="#121F4E" size={600} paddingBottom="10px">
              {this.props.review.title}
            </Heading>
          </Pane>
          <Pane width="50%">
            <Heading color="#121F4E" size={400}>
              {`${this.state.creator.first_name} ${this.state.creator.last_name}`}
            </Heading>
          </Pane>
        </Pane>
        <Pane width="60%">
          <Heading color="#121F4E" size={400} paddingBottom={10}>
            {this.props.review.content}
          </Heading>
        </Pane>
      </Pane>
    );
  }
}
