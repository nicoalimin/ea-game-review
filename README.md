# EA Game Review Platform

## Hosted Version on EKS

### Client

- http://ea-blog.nico-alimin.com

### Server

- Health Check: http://ea-blog.nico-alimin.com/api/v1/health
- Documentation: http://ea-blog.nico-alimin.com/api/v1/docs

## Running Locally

_Prerequisites:_

- docker (recommended `v19.03.5` and above)
- docker-compose (recommended `v1.24.1` and above)
- GNU Make (recommended `3.81` and above)

`docker-compose up`

### Client

open `localhost:8080` in your browser

### Server

`curl localhost:3031/health`

### Server Documentation

`localhost:3031/docs`

`https://gitlab.com/nicoalimin/ea-game-review/-/blob/master/server/docs/swagger.yaml`

## CI/CD

Using Gitlab CI/CD, the process is as follows:

- **Check generated code**: mocks, dependencies
- **Tests**: Lint, unit tests
- **Build**: Docker build, pushed to Dockerhub https://hub.docker.com/repository/docker/niczter/ea-server
- **Relase**: Deployed using Helm to EKS cluster hosted in AWS

Workflow:

- Single-environment, treated as Staging
- Continuous Deployment to Staging on merge to master and passing pipeline
- Example pipeline for one commit: https://gitlab.com/nicoalimin/ea-game-review/pipelines/130837821

## Postman

A Postman collection is available in `./EA-SERVER.postman_collection` to bootstrap development against the server

## Server Directory Structure

```
.
├── client                  - front-end
└── server
    ├── conf                - configuriation management
    ├── docs                - swagger docs
    ├── helm_kubectl_docker - custom docker image forhelm deployments
    ├── helmchart
    │   └── templates
    │       └── tests
    └── pkg
        ├── core            - business logic package
        ├── database        - persistence layer package
        │   └── memorydb    - in-memory database package
        ├── handler         - handler package
        ├── seed            - seeds the initial in-memory resources
        └── utils           - helpers package
```

# Contributing to the code

## Server

Please do the following steps to run the code:

- `cd server/`
- `go mod download`
- `go run .`

## Client

Please do the following steps:

- `cd client/`
- `npm install`
- `REACT_APP_BACKEND_URL="localhost:3031" npm start`

# Future Enhancements

- Implement Logging
- e2e tests
- pipeline performance enhancements (Use smaller Docker image)
- Add godoc
- Add ability to comment
